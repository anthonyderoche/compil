import ast.ASTException;
import ast.Node;
import code.CodeGenerator;
import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.DOTTreeGenerator;
import org.antlr.runtime.tree.Tree;
import org.antlr.stringtemplate.StringTemplate;
import semantic.SemanticControls;
import tds.TDSBuilder;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class Test {
	
	public static void usage(){
		System.out.println("Usage : file");
	}
	
	public static void main(String[] args) throws Exception {

	   //Tool.main(new String[]{"-o", "src", "src/LEAC.g"});
		
		if(args.length!=1){
			usage();
			System.exit(1);
		}

		FileInputStream file = new FileInputStream(args[0]);
		ANTLRInputStream input = new ANTLRInputStream(file);
		LEACLexer lexer = new LEACLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		LEACParser parser = new LEACParser(tokens);
		CommonTree tree = (CommonTree) parser.program().getTree();

		/*DOTTreeGenerator gen = new DOTTreeGenerator();
		StringTemplate st = gen.toDOT(tree);
		System.out.println(st);*/
		int syntaxErrors = parser.getNumberOfSyntaxErrors();
		if (syntaxErrors == 0) {

			Map<Integer, String> lexicon = buildLexicon(Test.class
					.getClassLoader().getResource("LEAC.tokens"));

			Node program = buildASTAndTDS(tree, lexicon);

			SemanticControls semanticControls = new SemanticControls(program);
			semanticControls.execute();

			if(semanticControls.getNumberOfSemanticErrors()==0){
	            CodeGenerator codeGenerator=new CodeGenerator(program);
	            codeGenerator.execute();
			}
		}
	}

	public static Node buildASTAndTDS(CommonTree tree,
			Map<Integer, String> lexicon) {
		TDSBuilder TDSBuilder = new TDSBuilder();
		Stack<Tree> stack = new Stack<Tree>();
		stack.add(tree);

		CommonTree child;

		Stack<Node> fathers = new Stack<Node>();
		Node program = new Node("program", "program", tree.getToken().getLine());
		program.setDepth(0);
		fathers.add(program);

		try {
			while (!stack.isEmpty()) {
				Tree first = stack.pop();
				Node father = fathers.pop();

				TDSBuilder.updateCurrent(father.getDepth());
				father.setTDS(TDSBuilder.getCurrentTDS());

				if (lexicon.get(first.getType()).equals("var")) {
					TDSBuilder.handleVar(first);
				}

				if (lexicon.get(first.getType()).equals("function")) {
					TDSBuilder.handleFunction(first, father.getDepth());
				}

				List<Node> sons = new ArrayList<Node>();
				for (int i = first.getChildCount() - 1; i >= 0; i--) {

					child = (CommonTree) first.getChild(i);
					stack.add(child);
					String label = lexicon.get(child.getType());
					String value = child.getText();
					if (label == null) {
						throw new ASTException(
								"ERROR IN LEXICON : missing type for "
										+ child.getText());
					} else {

						Node son = new Node(label, value, child.getToken()
								.getLine());

						son.setDepth(father.getDepth() + 1);
						fathers.add(son);
						sons.add(son);
					}

				}

				for (int j = sons.size() - 1; j >= 0; j--) {
					father.addChild(sons.get(j));
				}
			}
		} catch (ASTException e) {
			System.err.println(e.getMessage());
		}
		return program;
	}

	public static Map<Integer, String> buildLexicon(URL tokensFile)
			throws IOException {
		Map<Integer, String> lexicon = new HashMap<Integer, String>();

		Scanner scan = new Scanner(tokensFile.openStream());
		String key, value;
		while (scan.hasNext()) {
			String line = scan.nextLine();
			if (line.indexOf("T__") == -1) {
				int index = line.lastIndexOf('=');
				if (index > -1) {
					value = line.substring(0, index).replace("'", "");
					key = line.substring(index + 1);
					try {
						if (value.length() > 0 && key.length() > 0)
							lexicon.put(Integer.valueOf(key), value);
					} catch (NumberFormatException e) {
						System.err
								.println("Building lexicon : key is expected to be an integer. Found "
										+ key);
					}
				}
			}
		}
		scan.close();
		return lexicon;
	}
}
package code;

/**
 * Created by Xavier on 27/04/2014.
 */
public class Register {
    protected int reg;

    public Register(){
        this.reg=0;
    }

    public Register(int n){
        this.reg=n;
    }

    public int get() {
        return reg;
    }

    public int getPlus(int n) {
        return reg+n;
    }

    public int getMinus(int n) {
        return reg-n;
    }

    public int plus1Before(){
        return ++this.reg;
    }

    public int plus1After(){
        return this.reg++;
    }

    public int minus1Before(){
        return --this.reg;
    }

    public int minus1After(){
        return this.reg--;
    }

    public String toString(){
        return ""+ get();
    }
}

package code;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Xavier on 15/05/2014.
 */
public class FunctionsTable {

    protected HashMap<String,ArrayList<Boolean>> fonctions;

    public FunctionsTable(){
        this.fonctions=new HashMap<>();
    }

    public void addFunction(String function){
        this.fonctions.put(function,new ArrayList<Boolean>());
    }

    public void addArg(String function, boolean isRef){
        this.fonctions.get(function).add(isRef);
    }

    public void addArg(String function, boolean... isRef){
        for(boolean ref:isRef)
            this.fonctions.get(function).add(ref);
    }

    public boolean isRef(String function, int n){
        return this.fonctions.get(function).get(n);
    }
}

package code;

import ast.Node;
import tds.TDS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *  Created by Xavier on 22/04/2014.
 */
public class VariablesTable {

    protected HashMap<Integer,HashMap<String,ArrayList<String>>> variables;

    public VariablesTable(){
        variables=new HashMap<>();
    }

    public void addRegion(int n){
        if (variables.get(n) == null) {
            variables.put(n, new HashMap<String, ArrayList<String>>());
        }
    }

    public void addVariableToRegion(int n, String variable){
        variables.get(n).put(variable,new ArrayList<String>());
    }

    public void defineVariable(int n, String variable, String type, List<String> dimensions){
        ArrayList<String> s=variables.get(n).get(variable);
        s.clear();
        s.add(type+"_"+variable+n);
        s.add(type);
        for (int i = 0; i < dimensions.size(); i+=2) {
            s.add(dimensions.get(i));
            s.add(dimensions.get(i+1));
        }
    }

    public boolean contains(int n, String arg){
        return ((variables.get(n) != null) && (variables.get(n).get(arg) != null));
    }

    public String getAssemblyName(int n, String variable){
        return variables.get(n).get(variable).get(0);
    }

    public String getVariableInRegion(CodeGenerator cg, TDS tds, Node node, Register cur_reg){
        TDS nodetds = node.getTDS();
        while (nodetds.get(node.getValue()) == null) {
            nodetds = nodetds.getFather();
        }
        ArrayList<String> s=variables.get(nodetds.getRegion()).get(node.getValue());
        String out="";
        if(node.getChildCount()>0) {
            boolean onlyInt=true;
            for (Node n : node.getChildren()){
                try{
                    int i=Integer.parseInt(n.getValue());
                }catch (NumberFormatException nfe){
                    onlyInt=false;
                }
            }
            if(onlyInt) {
                out += "@(" + s.get(0);
                int deplacement = Integer.parseInt(node.getChild(0).getValue()) - Integer.parseInt(s.get(2));
                for (int i = 1; i < node.getChildCount(); i++) {
                    int tmp = Integer.parseInt(node.getChild(i).getValue()) - Integer.parseInt(s.get(2 + i * 2));
                    for (int j = 0; j < i; j++) {
                        tmp *= (Integer.parseInt(s.get(2 + j * 2 + 1)) - Integer.parseInt(s.get(2 + j * 2)) + 1);
                    }
                    deplacement += tmp;
                }
                switch (s.get(1)) {
                    case "int":
                        deplacement *= 2;
                        break;
                    case "bool":
                        deplacement *= 2;
                        break;
                    case "void":
                        deplacement *= 2;
                        break;
                }
                out += "+" + deplacement + ")";
            }else{
                out += "\t\t\tldw r" + cur_reg.plus1After() + ",#0\t\t//debut calcul position de "+s.get(0)+" avec le deplacement en nb d'elements\n\n";
                for (int i = 0; i < node.getChildCount(); i++) {
                    try{
                        int val=Integer.parseInt(node.getChild(i).getValue());
                        out += "\t\t\tldw r" + cur_reg.plus1After() + ",#" + val + "\n\n";
                    }catch (NumberFormatException nfe){
                        if(node.getChild(i).getLabel().equals("IDF")){
                            out+= "\t\t\tldw r" + cur_reg.plus1After() + "," + (this.contains(nodetds.getRegion(),node.getChild(i).getValue())?getVariableInRegion(cg,tds,node.getChild(i),cur_reg):cg.getArgs().getArgInRegion(cg,tds,node.getChild(i),cur_reg)) + "\n\n";
                        }else{
                            out+=cg.exprCodeGeneration(node.getChild(i));
                        }
                    }
                    for (int j = 0; j < i; j++) {
                        out += "\t\t\tldw r" + cur_reg.plus1After() + ",#" + (Integer.parseInt(s.get(2 + j * 2 + 1)) - Integer.parseInt(s.get(2 + j * 2)) + 1) + "\n\n";
                        out+="\t\t\tmul r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                    }
                    out+="\t\t\tadd r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                }
                switch (s.get(1)) {
                    case "int":
                        out+="\t\t\tldw r"+ cur_reg.plus1After()+",#2\t\t//taille d'un element\n\n";
                        out+="\t\t\tmul r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                        break;
                    case "bool":
                        out+="\t\t\tldw r"+ cur_reg.plus1After()+",#2\t\t//taille d'un element\n\n";
                        out+="\t\t\tmul r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                        break;
                    case "void":
                        out+="\t\t\tldw r"+ cur_reg.plus1After()+",#2\t\t//taille d'un element\n\n";
                        out+="\t\t\tmul r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                        break;
                }
                out += "\t\t\tldw r" + cur_reg.plus1After() + ",#" + s.get(0) + "\t\t//ajout de l'origine\n\n";
                out+="\t\t\tadd r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\t\t//fin du calcul\n\n";
                //cur_reg.minus1Before();
            }
        } else {
            out+="@"+s.get(0);
        }
        return out;
    }
}

package code;

import ast.Node;
import tds.TDS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *  Created by Xavier on 22/04/2014.
 */
public class ArgsTable {

    protected HashMap<Integer,HashMap<String,ArrayList<String>>> args;

    public ArgsTable(){
        args=new HashMap<>();
    }

    public void addRegion(int n){
        if (args.get(n) == null) {
            args.put(n, new HashMap<String, ArrayList<String>>());
        }
    }

    public void addArgToRegion(int n, String arg){
        args.get(n).put(arg,new ArrayList<String>());
    }

    public void defineArg(int n, String arg, boolean ref, String type, List<String> dimensions) {
        ArrayList<String> s = args.get(n).get(arg);
        s.clear();
        s.add(((ref)?"ref_":"agr_")+arg+n);
        s.add(ref ? "Y" : "N");
        s.add(type);
        for (int i = 0; i < dimensions.size(); i += 2) {
            s.add(dimensions.get(i));
            s.add(dimensions.get(i + 1));
        }
    }

    public boolean contains(int n, String arg){
        return ((args.get(n) != null) && (args.get(n).get(arg) != null));
    }

    public boolean isRef(int n,String arg){
        return args.get(n).get(arg).get(1).equals("Y");
    }

    public String getArgName(int n, String arg){
        return args.get(n).get(arg).get(0);
    }

    public String getArgInRegion(CodeGenerator cg,TDS tds, Node node, Register cur_reg){
        TDS nodetds = node.getTDS();
        /*while (nodetds.get(node.getValue()) == null) {
            nodetds = nodetds.getFather();
        }*/
        ArrayList<String> s=args.get(nodetds.getRegion()).get(node.getValue());
        String out="";
        if(node.getChildCount()>0) {
            boolean onlyInt=true;
            for (Node n : node.getChildren()){
                try{
                    int i=Integer.parseInt(n.getValue());
                }catch (NumberFormatException nfe){
                    onlyInt=false;
                }
            }
            if(onlyInt) {
                out += "\t\t\tldw r"+cur_reg+",SP\n\n";
                out += "\t\t\tadd r"+cur_reg+",#"+s.get(0)+"\n\n";
                int deplacement = Integer.parseInt(node.getChild(0).getValue()) - Integer.parseInt(s.get(3));
                for (int i = 1; i < node.getChildCount(); i++) {
                    int tmp = Integer.parseInt(node.getChild(i).getValue()) - Integer.parseInt(s.get(3 + i * 2));
                    for (int j = 0; j < i; j++) {
                        tmp *= (Integer.parseInt(s.get(3 + j * 2 + 1)) - Integer.parseInt(s.get(3 + j * 2)) + 1);
                    }
                    deplacement += tmp;
                }
                switch (s.get(2)) {
                    case "int":
                        deplacement *= 2;
                        break;
                    case "bool":
                        deplacement *= 2;
                        break;
                    case "void":
                        deplacement *= 2;
                        break;
                }
                out += "\t\t\tadd r"+cur_reg+", #"+ deplacement + "\n\n";
            }else{
                out += "\t\t\tldw r" + cur_reg.plus1After() + ",#0\t\t//debut calcul position de "+s.get(0)+" avec le deplacement en nb d'elements\n\n";
                for (int i = 0; i < node.getChildCount(); i++) {
                    try{
                        int val=Integer.parseInt(node.getChild(i).getValue());
                        out += "\t\t\tldw r" + cur_reg.plus1After() + ",#" + val + "\n\n";
                    }catch (NumberFormatException nfe){
                        if(node.getChild(i).getLabel().equals("IDF")){
                            out+= "\t\t\tldw r" + cur_reg.plus1After() + "," + (this.contains(nodetds.getRegion(),node.getChild(i).getValue())?getArgInRegion(cg,tds,node.getChild(i),cur_reg):cg.getIdentifiant().getVariableInRegion(cg,tds,node.getChild(i),cur_reg)) + "\n\n";
                        }else{
                            out+=cg.exprCodeGeneration(node.getChild(i));
                        }
                    }
                    for (int j = 0; j < i; j++) {
                        out += "\t\t\tldw r" + cur_reg.plus1After() + ",#" + (Integer.parseInt(s.get(3 + j * 2 + 1)) - Integer.parseInt(s.get(3 + j * 2)) + 1) + "\n\n";
                        out+="\t\t\tmul r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                    }
                    out+="\t\t\tadd r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                }
                switch (s.get(1)) {
                    case "int":
                        out+="\t\t\tldw r"+ cur_reg.plus1After()+",#2\t\t//taille d'un element\n\n";
                        out+="\t\t\tmul r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                        break;
                    case "bool":
                        out+="\t\t\tldw r"+ cur_reg.plus1After()+",#2\t\t//taille d'un element\n\n";
                        out+="\t\t\tmul r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                        break;
                    case "void":
                        out+="\t\t\tldw r"+ cur_reg.plus1After()+",#2\t\t//taille d'un element\n\n";
                        out+="\t\t\tmul r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                        break;
                }
                out += "\t\t\tldw r" + cur_reg.plus1After() + ",#" + s.get(0) + "\t\t//ajout de l'origine\n\n";
                out+="\t\t\tadd r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\t\t//fin du calcul\n\n";
            }
        } else {
            if(s.get(1).equals("Y")){
                out += "\t\t\tldw r"+cur_reg.plus1After()+",(BP)"+ s.get(0)+"\n\n";
                //out += "\t\t\tldw r"+cur_reg.plus1After()+",#"+ s.get(0)+"\n\n";
                //out+="\t\t\tadd r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                //cur_reg.minus1Before();
            } else {
                out += "(BP)" + s.get(0);
            }
        }
        return out;
    }
}

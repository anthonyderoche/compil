package code;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tds.TDS;
import ast.Node;

/**
 *  Created by Xavier on 29/03/2014.
 */
public class CodeGenerator {

    protected Node root;
    protected Register cur_reg;
    protected int if_cnt;
    protected int while_cnt;
    protected int string_cnt;
    protected VariablesTable identifiant;
    protected ArgsTable args;
    protected FunctionsTable fonctions;


    public CodeGenerator(Node root){
        this.root=root;
        this.cur_reg=new Register(1);
        this.if_cnt=0;
        this.while_cnt=0;
        this.string_cnt=0;
        this.identifiant=new VariablesTable();
        this.args=new ArgsTable();
        this.fonctions=new FunctionsTable();
    }

    public void execute(){
        Path out= Paths.get("out.txt");
        Charset charset = Charset.forName("UTF-8");
        Path header=null;
        Path systemFunction=null;
        try {
	        URI uriHeader = getClass().getClassLoader().getResource("code/Header.txt").toURI();
	        URI uriSystemFunction = getClass().getClassLoader().getResource("code/SystemFunctions.txt").toURI();
	        
	        
	        final Map<String, String> env = new HashMap<>();
	        if(uriHeader.toString().indexOf('!')>-1){
		        final String[] array1 = uriHeader.toString().split("!");
		        final String[] array2 = uriSystemFunction.toString().split("!");
		        final FileSystem fs = FileSystems.newFileSystem(URI.create(array1[0]), env);
		        header = fs.getPath(array1[1]);
		        systemFunction = fs.getPath(array2[1]);
	        }else{
	        	header = Paths.get(uriHeader);
	        	systemFunction = Paths.get(uriSystemFunction);
	        }

		} catch (IOException | URISyntaxException | NullPointerException e) {
			System.err.println("Problème de lecture des fichiers internes header.txt et SystemFunctions.txt");
			System.exit(1);
		} 
        
        //String s="test";
        try (BufferedWriter bw = Files.newBufferedWriter(out,charset)) {
            //System.out.println(rootChild.getLabel());
            for(Node rootChild : root.getChildren())
                switch (rootChild.getLabel()) {

                    /** Copy du header code **/

                    case "IDF": //
                        bw.write("// program " + rootChild + "\n");
                        try (BufferedReader br = Files.newBufferedReader(header, charset)) {
                            String line;
                            while ((line = br.readLine()) != null)
                                bw.write(line + "\n");
                        }
                        bw.write("\n");
                        break;

                    /** Declaration variables globales **/

                    case "VARDECLIST":
                        bw.write("buffer\t\trsb\t8\t\t//buffer pour write\n\n");
                        bw.write(vardecCodeGeneration(rootChild));
                        break;

                    /** Declaration fonctions **/

                    case "FUNDECLIST":
                        for (Node child:rootChild.getChildren()){
                            bw.write(functionCodeGeneration(child));
                        }
                        break;

                    /** Declaration du main **/

                    case "SEQUENCE":
                        bw.write("main_\n");
                        bw.write("\t\t\tldw SP,#STACK_ADRS\n\n");
                        bw.write("\t\t\tldq NIL,BP\n\n");
                        bw.write(sequenceCodeGeneration(rootChild));
                        break;
                }

            /**  Trape de fin de progamme **/

            bw.write("\t\t\ttrp #EXIT_EXC\t\t//fin du programme\n\n");
            try (BufferedReader br = Files.newBufferedReader(systemFunction, charset)) {
                String line;
                while ((line = br.readLine()) != null)
                    bw.write(line + "\n");
            }
            bw.write("\n");
            System.out.println("-> out.txt");
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }



    public String vardecCodeGeneration(Node root){
        String out="";
        identifiant.addRegion(root.getChild(0).getTDS().getRegion());
        for(Node rootChild : root.getChildren()){
            String type = rootChild.getChild(1).getValue();
            int allocation=1;
            ArrayList<String> dimensions=new ArrayList<>();
            if (type.equals("ARRAY")) {
                type = rootChild.getChild(1).getLastChild().getValue();
                for (int i=0;i<rootChild.getChild(1).getChildCount()-1;i++) {
                    dimensions.add(rootChild.getChildRecur(1, i, 0).getValue());//min
                    dimensions.add(rootChild.getChildRecur(1, i, 1).getValue());//max
                    allocation *= (Integer.parseInt(rootChild.getChildRecur(1, i, 1).getValue()) - Integer.parseInt(rootChild.getChildRecur(1, i, 0).getValue()) + 1);
                }
            }
            for (Node var : rootChild.getChild(0).getChildren()) {
                identifiant.addVariableToRegion(root.getTDS().getRegion(),var.getValue());
                identifiant.defineVariable(root.getTDS().getRegion(),var.getValue(),type,dimensions);
                out+=identifiant.getAssemblyName(root.getTDS().getRegion(), var.getValue());
                for (int k = identifiant.getAssemblyName(root.getTDS().getRegion(), var.getValue()).length(); k < 8; k += 4) {
                    out += "\t";
                }
                switch (type) {
                    case "int":
                        out+="\trsw\t\t" + allocation + "\n\n";
                        break;
                    case "bool":
                        out+="\trsw\t\t" + allocation + "\n\n";
                        break;
                    case "void":
                        out+="\trsw\t\t" + allocation + "\n\n";
                        break;
                }
            }
        }
        return out;
    }

    public String functionCodeGeneration(Node root){
        String out="";
        String name="";
        String save="",restore="";
        args.addRegion(root.getChild(0).getTDS().getRegion());
        for (Node child:root.getChildren()){
            switch (child.getLabel()) {
                case "IDF":
                    name = child.getValue();
                    fonctions.addFunction(name);
                    break;
                case "ARGLIST":
                    int deplacement = 4;
                    for (Node subchild : child.getChildren()) {
                        String type = subchild.getChild(1).getValue();
                        int allocation = 1;
                        ArrayList<String> dimensions = new ArrayList<>();
                        if (type.equals("ARRAY")) {
                            type = subchild.getChild(1).getLastChild().getValue();
                            for (int i = 0; i < subchild.getChild(1).getChildCount() - 1; i++) {
                                dimensions.add(subchild.getChildRecur(1, i, 0).getValue());//min
                                dimensions.add(subchild.getChildRecur(1, i, 1).getValue());//max
                                allocation *= (Integer.parseInt(subchild.getChildRecur(1, i, 1).getValue()) - Integer.parseInt(subchild.getChildRecur(1, i, 0).getValue()) + 1);
                            }
                        }
                        args.addArgToRegion(subchild.getTDS().getRegion(), subchild.getChild(0).getValue());
                        args.defineArg(subchild.getTDS().getRegion(), subchild.getChild(0).getValue(), subchild.getLabel().equals("ARGREF"), type, dimensions);
                        fonctions.addArg(name,subchild.getLabel().equals("ARGREF"));
                        out += args.getArgName(subchild.getTDS().getRegion(), subchild.getChild(0).getValue());
                        for (int k = args.getArgName(subchild.getTDS().getRegion(), subchild.getChild(0).getValue()).length(); k < 8; k += 4) {
                            out += "\t";
                        }
                        switch (type) {
                            case "int":
                                out += "\tEQU\t\t" + deplacement + "\n\n";
                                deplacement += allocation * 2;
                                break;
                            case "bool":
                                out += "\tEQU\t\t" + deplacement + "\n\n";
                                deplacement += allocation * 2;
                                break;
                            case "void":
                                out += "\tEQU\t\t" + deplacement + "\n\n";
                                deplacement += allocation * 2;
                                break;
                        }
                    }
                    break;
                case "VARDECLIST":
                    out += vardecCodeGeneration(child);
                    for(int i=0;i<child.getChildCount();i++) {
                        for (Node subchild : child.getChildRecur(i, 0).getChildren()) {
                            save += "\t\t\tldw r0," + identifiant.getVariableInRegion(this, subchild.getTDS(), subchild, cur_reg) + "\n\n";
                            save += "\t\t\tstw r0,-(SP)\n\n";
                        }
                    }
                    for(int j=child.getChildCount()-1;j>=0;j--) {
                        for (int i = child.getChildRecur(j, 0).getChildCount() - 1; i >= 0; i--) {
                            restore += "\t\t\tldw r0,(SP)+\n\n";
                            restore += "\t\t\tstw r0," + identifiant.getVariableInRegion(this, child.getChildRecur(j, 0, i).getTDS(), child.getChildRecur(j, 0, i), cur_reg) + "\n\n";
                        }
                    }
                    break;
                case "ATOMTYPE":
                    break;
                case "SEQUENCE":
                    out+=name+"_\n";
                    out+="\t\t\tstw BP, -(SP)\t\t// empile le contenu du registre BP\n\n";
                    out+="\t\t\tldw BP, SP\t\t// charge contenu SP ds BP qui pointe sur sa sauvegarde\n\n";
                    out+=save;
                    out+="\t\t\tstw r1, -(SP)\t\t//sauvegarde des registres\n\n";
                    out+="\t\t\tstw r2, -(SP)\n\n";
                    out+="\t\t\tstw r3, -(SP)\n\n";
                    out+="\t\t\tstw r4, -(SP)\n\n";
                    out+="\t\t\tstw r5, -(SP)\n\n";
                    out+="\t\t\tstw r6, -(SP)\n\n";
                    out+="\t\t\tstw r7, -(SP)\n\n";
                    out+="\t\t\tstw r8, -(SP)\n\n";
                    out+="\t\t\tstw r9, -(SP)\n\n";
                    out+="\t\t\tstw r10, -(SP)\n\n";
                    out+=sequenceCodeGeneration(child);
                    out+="\t\t\tldw r10, (SP)+\t\t//restoration des registres\n\n";
                    out+="\t\t\tldw r9, (SP)+\n\n";
                    out+="\t\t\tldw r8, (SP)+\n\n";
                    out+="\t\t\tldw r7, (SP)+\n\n";
                    out+="\t\t\tldw r6, (SP)+\n\n";
                    out+="\t\t\tldw r5, (SP)+\n\n";
                    out+="\t\t\tldw r4, (SP)+\n\n";
                    out+="\t\t\tldw r3, (SP)+\n\n";
                    out+="\t\t\tldw r2, (SP)+\n\n";
                    out+="\t\t\tldw r1, (SP)+\n\n";
                    out+=restore;
                    out+="\t\t\tldw BP, (SP)+\n\n";
                    out+="\t\t\tRTS\n\n";
            }
        }
        return out;
    }

    public String sequenceCodeGeneration(Node root){
        if(!root.getLabel().equals("SEQUENCE")){
            return instructionCodeGeneration(root);
        } else {
            String out="";
            for (Node rootChild : root.getChildren()){
                out+=sequenceCodeGeneration(rootChild);
            }
            return out;
        }
    }

    public String instructionCodeGeneration(Node root){
        String out="";
        switch (root.getLabel()){
            case "FUNCTION":
                for (int k=root.getChild(0).getChildCount()-1;k>=0;k--){
                    out+=exprCodeGeneration(root.getChild(0).getChild(k));
                    out += "\t\t\tstw r"+cur_reg.minus1Before()+", -(SP)\n\n";
                }
                out += "\t\t\tjsr @"+root.getChild(0).getValue()+"_\t\t// appelle fonction itoa d'adresse "+root.getChild(0).getValue()+"_\n\n";
                out += "\t\t\tadi SP, SP, #"+root.getChild(0).getChildCount()*2+"\n\n";
                if(!root.getFather().getLabel().equals("SEQUENCE")) {
                    out += "\t\t\tstw r0,r" + cur_reg.plus1After() + "\n\n";
                }
                break;
            case "if":
                int if_id=if_cnt++;
                out+="if"+if_id+"\n";
                out+=exprCodeGeneration(root.getChild(0));
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#0xffff\n\n";
                out+="\t\t\tcmp r" + cur_reg.minus1Before() + ",r"+cur_reg.minus1Before()+"\n\n";
                if(root.getChildCount()==3){
                    out+="\t\t\tjne #(else"+if_id+"-$-2)\n\n";
                    out+="then"+if_id+"\n";
                    out+=sequenceCodeGeneration(root.getChild(1));
                    out+="\t\t\tjmp #(fi"+if_id+"-$-2)\n\n";
                    out+="else"+if_id+"\n";
                    out+=sequenceCodeGeneration(root.getChild(2));
                } else {
                    out+="\t\t\tjne #(fi"+if_id+"-$-2)\n\n";
                    out+="then"+if_id+"\n";
                    out+=sequenceCodeGeneration(root.getChild(1));
                }
                out+="fi"+if_id+"\n";
                break;
            case "while":
                int while_id=while_cnt++;
                out+="while"+while_id+"\n";
                out+=exprCodeGeneration(root.getChild(0));
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#0xffff\n\n";
                out+="\t\t\tcmp r" + cur_reg.minus1Before() + ",r"+cur_reg.minus1Before()+"\n\n";
                out+="\t\t\tjne #(od"+while_id+"-$-2)\n\n";
                out+="do"+while_id+"\n";
                out+=sequenceCodeGeneration(root.getChild(1));
                out+="\t\t\tjmp #(while"+while_id+"-$-2)\n\n";
                out+="od"+while_id+"\n";
                break;
            case "=":
                out+=exprCodeGeneration(root.getChild(1));
                boolean onlyInt=true;
                for (Node n : root.getChild(0).getChildren()){
                    try{
                        int i=Integer.parseInt(n.getValue());
                    }catch (NumberFormatException nfe){
                        onlyInt=false;
                    }
                }
                if(onlyInt) {
                    if(args.contains(root.getTDS().getRegion(),root.getChild(0).getValue())){
                        String tmp="";
                        if(args.isRef(root.getTDS().getRegion(),root.getChild(0).getValue())){
                            out+=args.getArgInRegion(this, root.getChild(0).getTDS(), root.getChild(0), cur_reg);
                            tmp="(r" +cur_reg.getMinus(1) + ")";
                            cur_reg.minus1Before();
                        } else {
                            tmp=args.getArgInRegion(this, root.getChild(0).getTDS(), root.getChild(0), cur_reg);
                        }
                        out += "\t\t\tstw r" + cur_reg.minus1Before() +","+ tmp+"\n\n";
                    } else {
                        out += "\t\t\tstw r" + cur_reg.minus1Before() + "," + identifiant.getVariableInRegion(this, root.getChild(0).getTDS(), root.getChild(0), cur_reg) + "\n\n";
                    }
                } else {
                    if(args.contains(root.getTDS().getRegion(),root.getChild(0).getValue())){
                        out += args.getArgInRegion(this,root.getChild(0).getTDS(), root.getChild(0), cur_reg);
                        out += "\t\t\tstw r" + cur_reg.getMinus(2) + ",(r" + cur_reg.minus1Before() + ")\n\n";
                        cur_reg.minus1Before();
                    } else {
                        out += identifiant.getVariableInRegion(this,root.getChild(0).getTDS(), root.getChild(0), cur_reg);
                        out += "\t\t\tstw r" + cur_reg.getMinus(2) + ",(r" + cur_reg.minus1Before() + ")\n\n";
                        cur_reg.minus1Before();
                    }
                }
                break;
            case "return":
                out+=exprCodeGeneration(root.getChild(0));
                out+="\t\t\tstw r0,r"+cur_reg.minus1Before()+"\n\n";
                break;
            case "read":
                out += "\t\t\tldw r0" + ",#buffer\n\n";
                out+="\t\t\ttrp #READ_EXC\n\n";
                out += "\t\t\tstw r0, -(SP)\t\t// empile contenu de r0 (paramètre b)\n\n";
                out += "\t\t\tjsr @atoi_\t\t// appelle fonction atoi d'adresse atoi_\n\n";
                out += "\t\t\tadi SP, SP, #2\n\n";
                out += "\t\t\tstw r0,"+identifiant.getVariableInRegion(this,root.getTDS(),root.getChild(0),cur_reg)+"\n\n";
                break;
            case "write":
                switch (root.getChild(0).getLabel()) {
                    case "TXT":
                        out += "\t\t\tldw r0" + ",#string" + string_cnt + "\n\n";
                        out += "\t\t\ttrp #WRITE_EXC\n\n";
                        out += "\t\t\tjmp #(string" + string_cnt + "+" + ((root.getChild(0).getValue().length() % 2 == 0) ? root.getChild(0).getValue().length() : root.getChild(0).getValue().length() - 1) + "-$-2)\n\n";
                        out += "string" + string_cnt++ + "\t\tstring\t" + root.getChild(0).getValue() + "\n\n";
                        break;
                    case "INT":
                        out += "\t\t\tldw r0" + ",#string0\n\n";
                        out += "\t\t\ttrp #WRITE_EXC\n\n";
                        out += "\t\t\tjmp #(string" + string_cnt + "+" + ((root.getChild(0).getValue().length() % 2 == 0) ? root.getChild(0).getValue().length() + 2 : root.getChild(0).getValue().length() + 1) + "-$-2)\n\n";
                        out += "string" + string_cnt++ + "\t\tstring\t\"" + root.getChild(0).getValue() + "\"\n\n";
                        break;
                    case "true":
                        out += "\t\t\tldw r0" + ",#string" + string_cnt + "\n\n";
                        out += "\t\t\ttrp #WRITE_EXC\n\n";
                        out += "\t\t\tjmp #(string" + string_cnt + "+" + ((root.getChild(0).getValue().length() % 2 == 0) ? root.getChild(0).getValue().length() : root.getChild(0).getValue().length() - 1) + "-$-2)\n\n";
                        out += "string" + string_cnt++ + "\t\tstring\t\"" + root.getChild(0).getValue() + "\"\n\n";
                        break;
                    case "false":
                        out += "\t\t\tldw r0" + ",#string" + string_cnt + "\n\n";
                        out += "\t\t\ttrp #WRITE_EXC\n\n";
                        out += "\t\t\tjmp #(string" + string_cnt + "+" + ((root.getChild(0).getValue().length() % 2 == 0) ? root.getChild(0).getValue().length() : root.getChild(0).getValue().length() - 1) + "-$-2)\n\n";
                        out += "string" + string_cnt++ + "\t\tstring\t" + root.getChild(0).getValue() + "\n\n";
                        break;
                    case "IDF":
                        out += "\t\t\tldw r0, #10\t\t// charge 10 (pour base décimale) dans r0\n\n";
                        out += "\t\t\tstw r0, -(SP)\t\t// empile contenu de r0 (paramètre b)\n\n";
                        out += "\t\t\tldw r0, #buffer\t\t// adresse du buffer\n\n";
                        out += "\t\t\tstw r0, -(SP)\t\t// empile contenu de r0 (paramètre p)\n\n";
                        boolean onlyInt2 = true;
                        for (Node n : root.getChild(0).getChildren()) {
                            try {
                                int i = Integer.parseInt(n.getValue());
                            } catch (NumberFormatException nfe) {
                                onlyInt2 = false;
                            }
                        }
                        if (onlyInt2) {
                            if(args.contains(root.getTDS().getRegion(),root.getChild(0).getValue())){
                                String tmp="";
                                if(args.isRef(root.getTDS().getRegion(),root.getChild(0).getValue())){
                                    out+=args.getArgInRegion(this, root.getChild(0).getTDS(), root.getChild(0), cur_reg);
                                    tmp="(r" +cur_reg.getMinus(1) + ")";
                                    cur_reg.minus1Before();
                                } else {
                                    tmp=args.getArgInRegion(this, root.getChild(0).getTDS(), root.getChild(0), cur_reg);
                                }
                                out += "\t\t\tldw r0," + tmp + "\n\n";
                            } else {
                                out += "\t\t\tldw r0," + identifiant.getVariableInRegion(this, root.getChild(0).getTDS(), root.getChild(0), cur_reg) + "\n\n";
                            }
                        } else {
                            if(args.contains(root.getTDS().getRegion(),root.getChild(0).getValue())){
                                out += args.getArgInRegion(this,root.getChild(0).getTDS(), root.getChild(0), cur_reg);
                                out += "\t\t\tldw r0,(r" + cur_reg.minus1Before() + ")\n\n";
                            } else {
                                out += identifiant.getVariableInRegion(this, root.getChild(0).getTDS(), root.getChild(0), cur_reg);
                                out += "\t\t\tldw r0,(r" + cur_reg.minus1Before() + ")\n\n";
                            }
                        }
                        out += "\t\t\tstw r0, -(SP)\t\t// empile contenu de r0 (paramètre i)\n\n";
                        out += "\t\t\tjsr @itoa_\t\t// appelle fonction itoa d'adresse itoa_\n\n";
                        out += "\t\t\tadi SP, SP, #6\t\t// nettoie la pile des paramètres de taille totale 6 octets*/\n\n";
                        out += "\t\t\tldw r0, #buffer\t\t// adresse du buffer\n\n";
                        out += "\t\t\tstw r0, -(SP)\t\t// empile contenu de r0\n\n";
                        out += "\t\t\tjsr @print_\t\t// appelle fonction itoa d'adresse print_\n\n";
                        out += "\t\t\tadi SP, SP, #2\t\t// nettoie la pile des paramètres de taille totale 2 octets*/\n\n";
                        break;
                }
                break;
        }
        return out;
    }

    public String exprCodeGeneration(Node root){
        Node[] feuilles=new Node[2];
        String out="";
        if(!root.getLabel().equals("IDF")) {
            for (int i = 0; i < root.getChildCount(); i++) {
                if (root.getChild(i).getChildCount() > 0 && !root.getChild(i).getLabel().equals("IDF")) {
                    out += exprCodeGeneration(root.getChild(i));
                } else {
                    feuilles[i] = root.getChild(i);
                }
            }
        }
        for (Node feuille : feuilles) {
            if (feuille != null) {
                switch (feuille.getLabel()) {
                    case "INT":
                        out += "\t\t\tldw r" + cur_reg.plus1After() + ",#" + feuille.getValue() + "\n\n";
                        break;
                    case "IDF":
                        TDS tds = feuille.getTDS();
                        while (tds.get(feuille.getValue()) == null) {
                            tds = tds.getFather();
                        }
                        if(tds.get(feuille.getValue()).getCategory().equals("FUNCTION")){
                            for (int k=feuille.getChildCount()-1;k>=0;k--){
                                boolean onlyInt = true;
                                for (Node n : feuille.getChild(k).getChildren()) {
                                    try {
                                        int i = Integer.parseInt(n.getValue());
                                    } catch (NumberFormatException nfe) {
                                        onlyInt = false;
                                    }
                                }
                                if (onlyInt) {
                                    if(!feuille.getChild(k).getLabel().equals("IDF")){
                                        out+=exprCodeGeneration(root.getChild(k));
                                        out+="\t\t\tldw r0,r"+cur_reg.minus1Before()+"\n\n";
                                    }
                                    else if (args.contains(feuille.getChild(k).getTDS().getRegion(), feuille.getChild(k).getValue())) {
                                        String tmp="";
                                        if(args.isRef(feuille.getChild(k).getTDS().getRegion(),feuille.getChild(k).getValue())){
                                            out+=args.getArgInRegion(this, feuille.getChild(k).getTDS(), feuille.getChild(k), cur_reg);
                                            tmp="(r" +cur_reg.getMinus(1) + ")";
                                            cur_reg.minus1Before();
                                        } else {
                                            tmp=args.getArgInRegion(this, feuille.getChild(k).getTDS(), feuille.getChild(k), cur_reg);
                                        }
                                        out += "\t\t\tldw r0," + tmp + "\n\n";
                                    } else {
                                        out += "\t\t\tldw r0," + identifiant.getVariableInRegion(this, feuille.getChild(k).getTDS(), feuille.getChild(k), cur_reg) + "\n\n";
                                    }
                                } else {
                                    if(!feuille.getChild(k).getLabel().equals("IDF")){
                                        out+=exprCodeGeneration(root.getChild(k));
                                        out+="\t\t\tldw r0,r"+cur_reg.minus1Before()+"\n\n";
                                    }
                                    else if (args.contains(feuille.getChild(k).getTDS().getRegion(), feuille.getChild(k).getValue())) {
                                        out += args.getArgInRegion(this, feuille.getChild(k).getTDS(), feuille.getChild(k), cur_reg);
                                        String tmp="r" + cur_reg.minus1Before();
                                        if(!fonctions.isRef(feuille.getValue(),k)){
                                            tmp="("+tmp+")";
                                        }
                                        out += "\t\t\tldw r0," + tmp + "\n\n";
                                    } else {
                                        out += identifiant.getVariableInRegion(this, feuille.getChild(k).getTDS(), feuille.getChild(k), cur_reg);
                                        String tmp="r" + cur_reg.minus1Before();
                                        if(!fonctions.isRef(feuille.getValue(),k)){
                                            tmp="("+tmp+")";
                                        }
                                        out += "\t\t\tldw r0,"+ tmp + "\n\n";
                                    }
                                }
                                out += "\t\t\tstw r0, -(SP)\n\n";
                            }
                            out += "\t\t\tjsr @"+feuille.getValue()+"_\t\t// appelle fonction d'adresse "+feuille.getValue()+"_\n\n";
                            out += "\t\t\tadi SP, SP, #"+feuille.getChildCount()*2+"\n\n";
                            out+="\t\t\tstw r0,r"+cur_reg.plus1After()+"\n\n";
                        } else {
                            boolean onlyInt = true;
                            for (Node n : feuille.getChildren()) {
                                try {
                                    int i = Integer.parseInt(n.getValue());
                                } catch (NumberFormatException nfe) {
                                    onlyInt = false;
                                }
                            }
                            if (onlyInt) {
                                if (args.contains(feuille.getTDS().getRegion(), feuille.getValue())) {
                                    String tmp="";
                                    if(args.isRef(feuille.getTDS().getRegion(), feuille.getValue())){
                                        out+=args.getArgInRegion(this, feuille.getTDS(), feuille, cur_reg);
                                        tmp="(r" +cur_reg.getMinus(1) + ")";
                                        cur_reg.minus1Before();
                                    } else {
                                        tmp=args.getArgInRegion(this, feuille.getTDS(), feuille, cur_reg);
                                    }
                                    out += "\t\t\tldw r" + cur_reg.plus1After() + "," + tmp + "\n\n";
                                } else {
                                    out += "\t\t\tldw r" + cur_reg.plus1After() + "," + identifiant.getVariableInRegion(this, feuille.getTDS(), feuille, cur_reg) + "\n\n";
                                }
                            } else {
                                if (args.contains(root.getTDS().getRegion(), feuille.getValue())) {
                                    out += args.getArgInRegion(this, feuille.getTDS(), feuille, cur_reg);
                                    out += "\t\t\tldw r" + cur_reg.minus1Before() + ",(r" + cur_reg.plus1After() + ")\n\n";
                                } else {
                                    out += identifiant.getVariableInRegion(this, feuille.getTDS(), feuille, cur_reg);
                                    out += "\t\t\tldw r" + cur_reg.minus1Before() + ",(r" + cur_reg.plus1After() + ")\n\n";
                                }
                            }
                        }
                        break;
                    case "true":
                        out += "\t\t\tldw r" + cur_reg.plus1After() + ",#0xffff\n\n";
                        break;
                    case "false":
                        out += "\t\t\tldw r" + cur_reg.plus1After() + ",#0\n\n";
                        break;
                }
            }
        }
        //System.out.println(root.getLabel());
        switch (root.getLabel()) {
            case "+":
                out+="\t\t\tadd r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                break;
            case "-":
                out+="\t\t\tsub r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                break;
            case "*":
                out+="\t\t\tmul r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                break;
            case "/":
                out+="\t\t\tdiv r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r0\n\n";
                out+="\t\t\tstw r0,r"+cur_reg.getMinus(1)+"\n\n";
                break;
            case "and":
                out+="\t\t\tand r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                break;
            case "or":
                out+="\t\t\tor r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+",r"+cur_reg.getMinus(1)+"\n\n";
                break;
            case "not":
                out+="\t\t\tnot r" + cur_reg.minus1Before() + ",r"+cur_reg.plus1After()+"\n\n";
                break;
            case "<":
                out+="\t\t\tcmp r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+"\n\n";
                out+="\t\t\tblw 6\n\n";
                out+="\t\t\tldw r"+cur_reg.minus1Before()+",#0\n\n";
                out+="\t\t\tbmp 4\n\n";
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#0xffff\n\n";
                break;
            case "<=":
                out+="\t\t\tcmp r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+"\n\n";
                out+="\t\t\tble 6\n\n";
                out+="\t\t\tldw r"+cur_reg.minus1Before()+",#0\n\n";
                out+="\t\t\tbmp 4\n\n";
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#0xffff\n\n";
                break;
            case ">":
                out+="\t\t\tcmp r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+"\n\n";
                out+="\t\t\tbgt 6\n\n";
                out+="\t\t\tldw r"+cur_reg.minus1Before()+",#0\n\n";
                out+="\t\t\tbmp 4\n\n";
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#0xffff\n\n";
                break;
            case ">=":
                out+="\t\t\tcmp r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+"\n\n";
                out+="\t\t\tbge 6\n\n";
                out+="\t\t\tldw r"+cur_reg.minus1Before()+",#0\n\n";
                out+="\t\t\tbmp 4\n\n";
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#0xffff\n\n";
                break;
            case "==":
                out+="\t\t\tcmp r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+"\n\n";
                out+="\t\t\tbeq 6\n\n";
                out+="\t\t\tldw r"+cur_reg.minus1Before()+",#0\n\n";
                out+="\t\t\tbmp 4\n\n";
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#0xffff\n\n";
                break;
            case "!=":
                out+="\t\t\tcmp r" + cur_reg.getMinus(2) + ",r"+cur_reg.minus1Before()+"\n\n";
                out+="\t\t\tbne 6\n\n";
                out+="\t\t\tldw r"+cur_reg.minus1Before()+",#0\n\n";
                out+="\t\t\tbmp 4\n\n";
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#0xffff\n\n";
                break;
            case "INT":
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#"+root.getValue()+"\n\n";
                break;
            case "IDF":
                TDS tds = root.getTDS();
                while (tds.get(root.getValue()) == null) {
                    tds = tds.getFather();
                }
                if(tds.get(root.getValue()).getCategory().equals("FUNCTION")){
                    for (int k=root.getChildCount()-1;k>=0;k--){
                        boolean onlyInt = true;
                        for (Node n : root.getChild(k).getChildren()) {
                            try {
                                int i = Integer.parseInt(n.getValue());
                            } catch (NumberFormatException nfe) {
                                onlyInt = false;
                            }
                        }
                        if (onlyInt) {
                            if(!root.getChild(k).getLabel().equals("IDF")){
                                out+=exprCodeGeneration(root.getChild(k));
                                out+="\t\t\tldw r0,r"+cur_reg.minus1Before()+"\n\n";
                            }
                            else if (args.contains(root.getChild(k).getTDS().getRegion(), root.getChild(k).getValue())) {
                                String tmp="";
                                if(args.isRef(root.getChild(k).getTDS().getRegion(),root.getChild(k).getValue())){
                                    out+=args.getArgInRegion(this, root.getChild(k).getTDS(), root.getChild(k), cur_reg);
                                    tmp="(r" +cur_reg.getMinus(1) + ")";
                                    cur_reg.minus1Before();
                                } else {
                                    tmp=args.getArgInRegion(this, root.getChild(k).getTDS(), root.getChild(k), cur_reg);
                                }
                                out += "\t\t\tldw r0," + tmp + "\n\n";
                            } else {
                                out += "\t\t\tldw r0," + identifiant.getVariableInRegion(this, root.getChild(k).getTDS(), root.getChild(k), cur_reg) + "\n\n";
                            }
                        } else {
                            if(!root.getChild(k).getLabel().equals("IDF")){
                                out+=exprCodeGeneration(root.getChild(k));
                                out+="\t\t\tldw r0,r"+cur_reg.minus1Before()+"\n\n";
                            }
                            else if (args.contains(root.getChild(k).getTDS().getRegion(), root.getChild(k).getValue())) {
                                out += args.getArgInRegion(this, root.getChild(k).getTDS(), root.getChild(k), cur_reg);
                                String tmp="r" + cur_reg.minus1Before();
                                if(!fonctions.isRef(root.getValue(),k)){
                                    tmp="("+tmp+")";
                                }
                                out += "\t\t\tldw r0," + tmp + "\n\n";
                            } else {
                                out += identifiant.getVariableInRegion(this, root.getChild(k).getTDS(), root.getChild(k), cur_reg);
                                String tmp="r" + cur_reg.minus1Before();
                                if(!fonctions.isRef(root.getValue(),k)){
                                    tmp="("+tmp+")";
                                }
                                out += "\t\t\tldw r0,"+ tmp + "\n\n";
                            }
                        }
                        out += "\t\t\tstw r0, -(SP)\n\n";
                    }
                    out += "\t\t\tjsr @"+root.getValue()+"_\t\t// appelle fonction d'adresse "+root.getValue()+"_\n\n";
                    out += "\t\t\tadi SP, SP, #"+root.getChildCount()*2+"\n\n";
                    out+="\t\t\tstw r0,r"+cur_reg.plus1After()+"\n\n";
                } else {
                    boolean onlyInt = true;
                    for (Node n : root.getChildren()) {
                        try {
                            int i = Integer.parseInt(n.getValue());
                        } catch (NumberFormatException nfe) {
                            onlyInt = false;
                        }
                    }
                    if (onlyInt) {
                        if (args.contains(root.getTDS().getRegion(), root.getValue())) {
                            String tmp="";
                            if(args.isRef(root.getTDS().getRegion(), root.getValue())){
                                out+=args.getArgInRegion(this, root.getTDS(), root, cur_reg);
                                tmp="(r" +cur_reg.getMinus(1) + ")";
                                cur_reg.minus1Before();
                            } else {
                                tmp=args.getArgInRegion(this, root.getTDS(), root, cur_reg);
                            }
                            out += "\t\t\tldw r" + cur_reg.plus1After() + "," + tmp + "\n\n";
                        } else {
                            out += "\t\t\tldw r" + cur_reg.plus1After() + "," + identifiant.getVariableInRegion(this, root.getTDS(), root, cur_reg) + "\n\n";
                        }
                    } else {
                        if (args.contains(root.getTDS().getRegion(), root.getValue())) {
                            out += args.getArgInRegion(this, root.getTDS(), root, cur_reg);
                            out += "\t\t\tldw r" + cur_reg.minus1Before() + ",(r" + cur_reg.plus1After() + ")\n\n";
                        } else {
                            out += identifiant.getVariableInRegion(this, root.getTDS(), root, cur_reg);
                            out += "\t\t\tldw r" + cur_reg.minus1Before() + ",(r" + cur_reg.plus1After() + ")\n\n";
                        }
                    }
                }
                break;
            case "true":
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#0xffff\n\n";
                break;
            case "false":
                out+="\t\t\tldw r"+cur_reg.plus1After()+",#0\n\n";
                break;
        }
        return out;
    }

    public VariablesTable getIdentifiant(){
        return identifiant;
    }

    public ArgsTable getArgs(){
        return args;
    }
}

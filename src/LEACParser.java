// $ANTLR 3.2 Sep 23, 2009 12:02:23 src/LEAC.g 2014-04-28 14:33:44

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;

public class LEACParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "VARDECLIST", "IDENTLIST", "FUNDECLIST", "RANGELIST", "ARGLIST", "ARG", "ARGREF", "FUNCTION", "SEQUENCE", "ARRAY", "IDF", "ATOMTYPE", "INT", "TXT", "COMMENTAIRE", "NEWLINE", "WS", "'program'", "'var'", "':'", "';'", "','", "'array'", "'['", "']'", "'of'", "'..'", "'function'", "'('", "')'", "'ref'", "'if'", "'then'", "'else'", "'while'", "'do'", "'='", "'return'", "'read'", "'write'", "'{'", "'}'", "'or'", "'and'", "'<'", "'<='", "'>'", "'>='", "'=='", "'!='", "'+'", "'-'", "'*'", "'/'", "'^'", "'not'", "'true'", "'false'"
    };
    public static final int FUNCTION=11;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int IDF=14;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int T__21=21;
    public static final int ARGLIST=8;
    public static final int T__61=61;
    public static final int T__60=60;
    public static final int EOF=-1;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int ARG=9;
    public static final int IDENTLIST=5;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int TXT=17;
    public static final int T__59=59;
    public static final int ATOMTYPE=15;
    public static final int T__50=50;
    public static final int ARRAY=13;
    public static final int FUNDECLIST=6;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RANGELIST=7;
    public static final int INT=16;
    public static final int SEQUENCE=12;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int WS=20;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int NEWLINE=19;
    public static final int T__35=35;
    public static final int COMMENTAIRE=18;
    public static final int T__36=36;
    public static final int ARGREF=10;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int VARDECLIST=4;

    // delegates
    // delegators


        public LEACParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public LEACParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return LEACParser.tokenNames; }
    public String getGrammarFileName() { return "src/LEAC.g"; }


    public static class program_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "program"
    // src/LEAC.g:6:1: program : 'program' IDF vardeclist fundeclist ( instr | instrsp ) ;
    public final LEACParser.program_return program() throws RecognitionException {
        LEACParser.program_return retval = new LEACParser.program_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal1=null;
        Token IDF2=null;
        LEACParser.vardeclist_return vardeclist3 = null;

        LEACParser.fundeclist_return fundeclist4 = null;

        LEACParser.instr_return instr5 = null;

        LEACParser.instrsp_return instrsp6 = null;


        Object string_literal1_tree=null;
        Object IDF2_tree=null;

        try {
            // src/LEAC.g:6:8: ( 'program' IDF vardeclist fundeclist ( instr | instrsp ) )
            // src/LEAC.g:6:10: 'program' IDF vardeclist fundeclist ( instr | instrsp )
            {
            root_0 = (Object)adaptor.nil();

            string_literal1=(Token)match(input,21,FOLLOW_21_in_program47); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal1_tree = (Object)adaptor.create(string_literal1);
            root_0 = (Object)adaptor.becomeRoot(string_literal1_tree, root_0);
            }
            IDF2=(Token)match(input,IDF,FOLLOW_IDF_in_program50); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDF2_tree = (Object)adaptor.create(IDF2);
            adaptor.addChild(root_0, IDF2_tree);
            }
            pushFollow(FOLLOW_vardeclist_in_program52);
            vardeclist3=vardeclist();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, vardeclist3.getTree());
            pushFollow(FOLLOW_fundeclist_in_program54);
            fundeclist4=fundeclist();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, fundeclist4.getTree());
            // src/LEAC.g:6:47: ( instr | instrsp )
            int alt1=2;
            switch ( input.LA(1) ) {
            case 35:
                {
                int LA1_1 = input.LA(2);

                if ( (synpred1_LEAC()) ) {
                    alt1=1;
                }
                else if ( (true) ) {
                    alt1=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
                }
                break;
            case 38:
                {
                int LA1_2 = input.LA(2);

                if ( (synpred1_LEAC()) ) {
                    alt1=1;
                }
                else if ( (true) ) {
                    alt1=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 2, input);

                    throw nvae;
                }
                }
                break;
            case IDF:
            case 41:
            case 42:
            case 43:
                {
                alt1=1;
                }
                break;
            case 44:
                {
                alt1=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // src/LEAC.g:6:48: instr
                    {
                    pushFollow(FOLLOW_instr_in_program57);
                    instr5=instr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, instr5.getTree());

                    }
                    break;
                case 2 :
                    // src/LEAC.g:6:54: instrsp
                    {
                    pushFollow(FOLLOW_instrsp_in_program59);
                    instrsp6=instrsp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, instrsp6.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "program"

    public static class vardeclist_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "vardeclist"
    // src/LEAC.g:8:1: vardeclist : ( ( varsuitdecl )+ -> ^( VARDECLIST ( varsuitdecl )+ ) )? ;
    public final LEACParser.vardeclist_return vardeclist() throws RecognitionException {
        LEACParser.vardeclist_return retval = new LEACParser.vardeclist_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        LEACParser.varsuitdecl_return varsuitdecl7 = null;


        RewriteRuleSubtreeStream stream_varsuitdecl=new RewriteRuleSubtreeStream(adaptor,"rule varsuitdecl");
        try {
            // src/LEAC.g:8:12: ( ( ( varsuitdecl )+ -> ^( VARDECLIST ( varsuitdecl )+ ) )? )
            // src/LEAC.g:8:14: ( ( varsuitdecl )+ -> ^( VARDECLIST ( varsuitdecl )+ ) )?
            {
            // src/LEAC.g:8:14: ( ( varsuitdecl )+ -> ^( VARDECLIST ( varsuitdecl )+ ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==22) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // src/LEAC.g:8:15: ( varsuitdecl )+
                    {
                    // src/LEAC.g:8:15: ( varsuitdecl )+
                    int cnt2=0;
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==22) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // src/LEAC.g:0:0: varsuitdecl
                    	    {
                    	    pushFollow(FOLLOW_varsuitdecl_in_vardeclist69);
                    	    varsuitdecl7=varsuitdecl();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) stream_varsuitdecl.add(varsuitdecl7.getTree());

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt2 >= 1 ) break loop2;
                    	    if (state.backtracking>0) {state.failed=true; return retval;}
                                EarlyExitException eee =
                                    new EarlyExitException(2, input);
                                throw eee;
                        }
                        cnt2++;
                    } while (true);



                    // AST REWRITE
                    // elements: varsuitdecl
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if ( state.backtracking==0 ) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 8:28: -> ^( VARDECLIST ( varsuitdecl )+ )
                    {
                        // src/LEAC.g:8:31: ^( VARDECLIST ( varsuitdecl )+ )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VARDECLIST, "VARDECLIST"), root_1);

                        if ( !(stream_varsuitdecl.hasNext()) ) {
                            throw new RewriteEarlyExitException();
                        }
                        while ( stream_varsuitdecl.hasNext() ) {
                            adaptor.addChild(root_1, stream_varsuitdecl.nextTree());

                        }
                        stream_varsuitdecl.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;}
                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "vardeclist"

    public static class varsuitdecl_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "varsuitdecl"
    // src/LEAC.g:10:1: varsuitdecl : 'var' identlist ':' typename ';' ;
    public final LEACParser.varsuitdecl_return varsuitdecl() throws RecognitionException {
        LEACParser.varsuitdecl_return retval = new LEACParser.varsuitdecl_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal8=null;
        Token char_literal10=null;
        Token char_literal12=null;
        LEACParser.identlist_return identlist9 = null;

        LEACParser.typename_return typename11 = null;


        Object string_literal8_tree=null;
        Object char_literal10_tree=null;
        Object char_literal12_tree=null;

        try {
            // src/LEAC.g:10:13: ( 'var' identlist ':' typename ';' )
            // src/LEAC.g:10:15: 'var' identlist ':' typename ';'
            {
            root_0 = (Object)adaptor.nil();

            string_literal8=(Token)match(input,22,FOLLOW_22_in_varsuitdecl89); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal8_tree = (Object)adaptor.create(string_literal8);
            root_0 = (Object)adaptor.becomeRoot(string_literal8_tree, root_0);
            }
            pushFollow(FOLLOW_identlist_in_varsuitdecl92);
            identlist9=identlist();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, identlist9.getTree());
            char_literal10=(Token)match(input,23,FOLLOW_23_in_varsuitdecl94); if (state.failed) return retval;
            pushFollow(FOLLOW_typename_in_varsuitdecl97);
            typename11=typename();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, typename11.getTree());
            char_literal12=(Token)match(input,24,FOLLOW_24_in_varsuitdecl99); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "varsuitdecl"

    public static class identlist_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "identlist"
    // src/LEAC.g:12:1: identlist : IDF ( ',' IDF )* -> ^( IDENTLIST ( IDF )+ ) ;
    public final LEACParser.identlist_return identlist() throws RecognitionException {
        LEACParser.identlist_return retval = new LEACParser.identlist_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token IDF13=null;
        Token char_literal14=null;
        Token IDF15=null;

        Object IDF13_tree=null;
        Object char_literal14_tree=null;
        Object IDF15_tree=null;
        RewriteRuleTokenStream stream_IDF=new RewriteRuleTokenStream(adaptor,"token IDF");
        RewriteRuleTokenStream stream_25=new RewriteRuleTokenStream(adaptor,"token 25");

        try {
            // src/LEAC.g:12:11: ( IDF ( ',' IDF )* -> ^( IDENTLIST ( IDF )+ ) )
            // src/LEAC.g:12:13: IDF ( ',' IDF )*
            {
            IDF13=(Token)match(input,IDF,FOLLOW_IDF_in_identlist108); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_IDF.add(IDF13);

            // src/LEAC.g:12:17: ( ',' IDF )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==25) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // src/LEAC.g:12:18: ',' IDF
            	    {
            	    char_literal14=(Token)match(input,25,FOLLOW_25_in_identlist111); if (state.failed) return retval; 
            	    if ( state.backtracking==0 ) stream_25.add(char_literal14);

            	    IDF15=(Token)match(input,IDF,FOLLOW_IDF_in_identlist113); if (state.failed) return retval; 
            	    if ( state.backtracking==0 ) stream_IDF.add(IDF15);


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);



            // AST REWRITE
            // elements: IDF
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 12:28: -> ^( IDENTLIST ( IDF )+ )
            {
                // src/LEAC.g:12:31: ^( IDENTLIST ( IDF )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(IDENTLIST, "IDENTLIST"), root_1);

                if ( !(stream_IDF.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_IDF.hasNext() ) {
                    adaptor.addChild(root_1, stream_IDF.nextNode());

                }
                stream_IDF.reset();

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;}
            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "identlist"

    public static class typename_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "typename"
    // src/LEAC.g:14:1: typename : ( ATOMTYPE | arraytype );
    public final LEACParser.typename_return typename() throws RecognitionException {
        LEACParser.typename_return retval = new LEACParser.typename_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token ATOMTYPE16=null;
        LEACParser.arraytype_return arraytype17 = null;


        Object ATOMTYPE16_tree=null;

        try {
            // src/LEAC.g:14:10: ( ATOMTYPE | arraytype )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==ATOMTYPE) ) {
                alt5=1;
            }
            else if ( (LA5_0==26) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // src/LEAC.g:14:12: ATOMTYPE
                    {
                    root_0 = (Object)adaptor.nil();

                    ATOMTYPE16=(Token)match(input,ATOMTYPE,FOLLOW_ATOMTYPE_in_typename132); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    ATOMTYPE16_tree = (Object)adaptor.create(ATOMTYPE16);
                    adaptor.addChild(root_0, ATOMTYPE16_tree);
                    }

                    }
                    break;
                case 2 :
                    // src/LEAC.g:14:21: arraytype
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_arraytype_in_typename134);
                    arraytype17=arraytype();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, arraytype17.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "typename"

    public static class arraytype_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "arraytype"
    // src/LEAC.g:18:1: arraytype : 'array' '[' rangelist ( ',' rangelist )* ']' 'of' ATOMTYPE -> ^( ARRAY ( rangelist )+ ATOMTYPE ) ;
    public final LEACParser.arraytype_return arraytype() throws RecognitionException {
        LEACParser.arraytype_return retval = new LEACParser.arraytype_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal18=null;
        Token char_literal19=null;
        Token char_literal21=null;
        Token char_literal23=null;
        Token string_literal24=null;
        Token ATOMTYPE25=null;
        LEACParser.rangelist_return rangelist20 = null;

        LEACParser.rangelist_return rangelist22 = null;


        Object string_literal18_tree=null;
        Object char_literal19_tree=null;
        Object char_literal21_tree=null;
        Object char_literal23_tree=null;
        Object string_literal24_tree=null;
        Object ATOMTYPE25_tree=null;
        RewriteRuleTokenStream stream_ATOMTYPE=new RewriteRuleTokenStream(adaptor,"token ATOMTYPE");
        RewriteRuleTokenStream stream_25=new RewriteRuleTokenStream(adaptor,"token 25");
        RewriteRuleTokenStream stream_26=new RewriteRuleTokenStream(adaptor,"token 26");
        RewriteRuleTokenStream stream_27=new RewriteRuleTokenStream(adaptor,"token 27");
        RewriteRuleTokenStream stream_28=new RewriteRuleTokenStream(adaptor,"token 28");
        RewriteRuleTokenStream stream_29=new RewriteRuleTokenStream(adaptor,"token 29");
        RewriteRuleSubtreeStream stream_rangelist=new RewriteRuleSubtreeStream(adaptor,"rule rangelist");
        try {
            // src/LEAC.g:18:11: ( 'array' '[' rangelist ( ',' rangelist )* ']' 'of' ATOMTYPE -> ^( ARRAY ( rangelist )+ ATOMTYPE ) )
            // src/LEAC.g:18:13: 'array' '[' rangelist ( ',' rangelist )* ']' 'of' ATOMTYPE
            {
            string_literal18=(Token)match(input,26,FOLLOW_26_in_arraytype157); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_26.add(string_literal18);

            char_literal19=(Token)match(input,27,FOLLOW_27_in_arraytype159); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_27.add(char_literal19);

            pushFollow(FOLLOW_rangelist_in_arraytype161);
            rangelist20=rangelist();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) stream_rangelist.add(rangelist20.getTree());
            // src/LEAC.g:18:35: ( ',' rangelist )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==25) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // src/LEAC.g:18:36: ',' rangelist
            	    {
            	    char_literal21=(Token)match(input,25,FOLLOW_25_in_arraytype164); if (state.failed) return retval; 
            	    if ( state.backtracking==0 ) stream_25.add(char_literal21);

            	    pushFollow(FOLLOW_rangelist_in_arraytype166);
            	    rangelist22=rangelist();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) stream_rangelist.add(rangelist22.getTree());

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            char_literal23=(Token)match(input,28,FOLLOW_28_in_arraytype170); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_28.add(char_literal23);

            string_literal24=(Token)match(input,29,FOLLOW_29_in_arraytype172); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_29.add(string_literal24);

            ATOMTYPE25=(Token)match(input,ATOMTYPE,FOLLOW_ATOMTYPE_in_arraytype174); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_ATOMTYPE.add(ATOMTYPE25);



            // AST REWRITE
            // elements: rangelist, ATOMTYPE
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 18:70: -> ^( ARRAY ( rangelist )+ ATOMTYPE )
            {
                // src/LEAC.g:18:72: ^( ARRAY ( rangelist )+ ATOMTYPE )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ARRAY, "ARRAY"), root_1);

                if ( !(stream_rangelist.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_rangelist.hasNext() ) {
                    adaptor.addChild(root_1, stream_rangelist.nextTree());

                }
                stream_rangelist.reset();
                adaptor.addChild(root_1, stream_ATOMTYPE.nextNode());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;}
            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "arraytype"

    public static class rangelist_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "rangelist"
    // src/LEAC.g:20:1: rangelist : INT '..' INT -> ^( RANGELIST INT INT ) ;
    public final LEACParser.rangelist_return rangelist() throws RecognitionException {
        LEACParser.rangelist_return retval = new LEACParser.rangelist_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token INT26=null;
        Token string_literal27=null;
        Token INT28=null;

        Object INT26_tree=null;
        Object string_literal27_tree=null;
        Object INT28_tree=null;
        RewriteRuleTokenStream stream_INT=new RewriteRuleTokenStream(adaptor,"token INT");
        RewriteRuleTokenStream stream_30=new RewriteRuleTokenStream(adaptor,"token 30");

        try {
            // src/LEAC.g:20:11: ( INT '..' INT -> ^( RANGELIST INT INT ) )
            // src/LEAC.g:20:13: INT '..' INT
            {
            INT26=(Token)match(input,INT,FOLLOW_INT_in_rangelist192); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_INT.add(INT26);

            string_literal27=(Token)match(input,30,FOLLOW_30_in_rangelist194); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_30.add(string_literal27);

            INT28=(Token)match(input,INT,FOLLOW_INT_in_rangelist196); if (state.failed) return retval; 
            if ( state.backtracking==0 ) stream_INT.add(INT28);



            // AST REWRITE
            // elements: INT, INT
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 20:26: -> ^( RANGELIST INT INT )
            {
                // src/LEAC.g:20:29: ^( RANGELIST INT INT )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RANGELIST, "RANGELIST"), root_1);

                adaptor.addChild(root_1, stream_INT.nextNode());
                adaptor.addChild(root_1, stream_INT.nextNode());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;}
            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "rangelist"

    public static class fundeclist_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "fundeclist"
    // src/LEAC.g:22:1: fundeclist : ( ( fundecl )+ -> ^( FUNDECLIST ( fundecl )+ ) )? ;
    public final LEACParser.fundeclist_return fundeclist() throws RecognitionException {
        LEACParser.fundeclist_return retval = new LEACParser.fundeclist_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        LEACParser.fundecl_return fundecl29 = null;


        RewriteRuleSubtreeStream stream_fundecl=new RewriteRuleSubtreeStream(adaptor,"rule fundecl");
        try {
            // src/LEAC.g:22:12: ( ( ( fundecl )+ -> ^( FUNDECLIST ( fundecl )+ ) )? )
            // src/LEAC.g:22:14: ( ( fundecl )+ -> ^( FUNDECLIST ( fundecl )+ ) )?
            {
            // src/LEAC.g:22:14: ( ( fundecl )+ -> ^( FUNDECLIST ( fundecl )+ ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==31) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // src/LEAC.g:22:15: ( fundecl )+
                    {
                    // src/LEAC.g:22:15: ( fundecl )+
                    int cnt7=0;
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==31) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // src/LEAC.g:22:16: fundecl
                    	    {
                    	    pushFollow(FOLLOW_fundecl_in_fundeclist216);
                    	    fundecl29=fundecl();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) stream_fundecl.add(fundecl29.getTree());

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt7 >= 1 ) break loop7;
                    	    if (state.backtracking>0) {state.failed=true; return retval;}
                                EarlyExitException eee =
                                    new EarlyExitException(7, input);
                                throw eee;
                        }
                        cnt7++;
                    } while (true);



                    // AST REWRITE
                    // elements: fundecl
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if ( state.backtracking==0 ) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 22:26: -> ^( FUNDECLIST ( fundecl )+ )
                    {
                        // src/LEAC.g:22:29: ^( FUNDECLIST ( fundecl )+ )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(FUNDECLIST, "FUNDECLIST"), root_1);

                        if ( !(stream_fundecl.hasNext()) ) {
                            throw new RewriteEarlyExitException();
                        }
                        while ( stream_fundecl.hasNext() ) {
                            adaptor.addChild(root_1, stream_fundecl.nextTree());

                        }
                        stream_fundecl.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;}
                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "fundeclist"

    public static class fundecl_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "fundecl"
    // src/LEAC.g:24:1: fundecl : 'function' IDF '(' arglist ')' ':' ATOMTYPE vardeclist ( instr | instrsp ) ;
    public final LEACParser.fundecl_return fundecl() throws RecognitionException {
        LEACParser.fundecl_return retval = new LEACParser.fundecl_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal30=null;
        Token IDF31=null;
        Token char_literal32=null;
        Token char_literal34=null;
        Token char_literal35=null;
        Token ATOMTYPE36=null;
        LEACParser.arglist_return arglist33 = null;

        LEACParser.vardeclist_return vardeclist37 = null;

        LEACParser.instr_return instr38 = null;

        LEACParser.instrsp_return instrsp39 = null;


        Object string_literal30_tree=null;
        Object IDF31_tree=null;
        Object char_literal32_tree=null;
        Object char_literal34_tree=null;
        Object char_literal35_tree=null;
        Object ATOMTYPE36_tree=null;

        try {
            // src/LEAC.g:24:9: ( 'function' IDF '(' arglist ')' ':' ATOMTYPE vardeclist ( instr | instrsp ) )
            // src/LEAC.g:24:11: 'function' IDF '(' arglist ')' ':' ATOMTYPE vardeclist ( instr | instrsp )
            {
            root_0 = (Object)adaptor.nil();

            string_literal30=(Token)match(input,31,FOLLOW_31_in_fundecl237); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            string_literal30_tree = (Object)adaptor.create(string_literal30);
            root_0 = (Object)adaptor.becomeRoot(string_literal30_tree, root_0);
            }
            IDF31=(Token)match(input,IDF,FOLLOW_IDF_in_fundecl240); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDF31_tree = (Object)adaptor.create(IDF31);
            adaptor.addChild(root_0, IDF31_tree);
            }
            char_literal32=(Token)match(input,32,FOLLOW_32_in_fundecl242); if (state.failed) return retval;
            pushFollow(FOLLOW_arglist_in_fundecl245);
            arglist33=arglist();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, arglist33.getTree());
            char_literal34=(Token)match(input,33,FOLLOW_33_in_fundecl247); if (state.failed) return retval;
            char_literal35=(Token)match(input,23,FOLLOW_23_in_fundecl250); if (state.failed) return retval;
            ATOMTYPE36=(Token)match(input,ATOMTYPE,FOLLOW_ATOMTYPE_in_fundecl253); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            ATOMTYPE36_tree = (Object)adaptor.create(ATOMTYPE36);
            adaptor.addChild(root_0, ATOMTYPE36_tree);
            }
            pushFollow(FOLLOW_vardeclist_in_fundecl255);
            vardeclist37=vardeclist();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, vardeclist37.getTree());
            // src/LEAC.g:24:70: ( instr | instrsp )
            int alt9=2;
            switch ( input.LA(1) ) {
            case 35:
                {
                int LA9_1 = input.LA(2);

                if ( (synpred9_LEAC()) ) {
                    alt9=1;
                }
                else if ( (true) ) {
                    alt9=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae;
                }
                }
                break;
            case 38:
                {
                int LA9_2 = input.LA(2);

                if ( (synpred9_LEAC()) ) {
                    alt9=1;
                }
                else if ( (true) ) {
                    alt9=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 2, input);

                    throw nvae;
                }
                }
                break;
            case IDF:
            case 41:
            case 42:
            case 43:
                {
                alt9=1;
                }
                break;
            case 44:
                {
                alt9=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // src/LEAC.g:24:71: instr
                    {
                    pushFollow(FOLLOW_instr_in_fundecl258);
                    instr38=instr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, instr38.getTree());

                    }
                    break;
                case 2 :
                    // src/LEAC.g:24:77: instrsp
                    {
                    pushFollow(FOLLOW_instrsp_in_fundecl260);
                    instrsp39=instrsp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, instrsp39.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "fundecl"

    public static class arglist_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "arglist"
    // src/LEAC.g:26:1: arglist : ( arg ( ',' arg )* -> ^( ARGLIST ( arg )+ ) )? ;
    public final LEACParser.arglist_return arglist() throws RecognitionException {
        LEACParser.arglist_return retval = new LEACParser.arglist_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal41=null;
        LEACParser.arg_return arg40 = null;

        LEACParser.arg_return arg42 = null;


        Object char_literal41_tree=null;
        RewriteRuleTokenStream stream_25=new RewriteRuleTokenStream(adaptor,"token 25");
        RewriteRuleSubtreeStream stream_arg=new RewriteRuleSubtreeStream(adaptor,"rule arg");
        try {
            // src/LEAC.g:26:9: ( ( arg ( ',' arg )* -> ^( ARGLIST ( arg )+ ) )? )
            // src/LEAC.g:26:11: ( arg ( ',' arg )* -> ^( ARGLIST ( arg )+ ) )?
            {
            // src/LEAC.g:26:11: ( arg ( ',' arg )* -> ^( ARGLIST ( arg )+ ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==IDF||LA11_0==34) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // src/LEAC.g:26:12: arg ( ',' arg )*
                    {
                    pushFollow(FOLLOW_arg_in_arglist271);
                    arg40=arg();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) stream_arg.add(arg40.getTree());
                    // src/LEAC.g:26:16: ( ',' arg )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==25) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // src/LEAC.g:26:17: ',' arg
                    	    {
                    	    char_literal41=(Token)match(input,25,FOLLOW_25_in_arglist274); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_25.add(char_literal41);

                    	    pushFollow(FOLLOW_arg_in_arglist276);
                    	    arg42=arg();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) stream_arg.add(arg42.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);



                    // AST REWRITE
                    // elements: arg
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if ( state.backtracking==0 ) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 26:27: -> ^( ARGLIST ( arg )+ )
                    {
                        // src/LEAC.g:26:30: ^( ARGLIST ( arg )+ )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ARGLIST, "ARGLIST"), root_1);

                        if ( !(stream_arg.hasNext()) ) {
                            throw new RewriteEarlyExitException();
                        }
                        while ( stream_arg.hasNext() ) {
                            adaptor.addChild(root_1, stream_arg.nextTree());

                        }
                        stream_arg.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;}
                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "arglist"

    public static class arg_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "arg"
    // src/LEAC.g:28:1: arg : ( IDF ':' typename -> ^( ARG IDF typename ) | 'ref' IDF ':' typename -> ^( ARGREF IDF typename ) );
    public final LEACParser.arg_return arg() throws RecognitionException {
        LEACParser.arg_return retval = new LEACParser.arg_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token IDF43=null;
        Token char_literal44=null;
        Token string_literal46=null;
        Token IDF47=null;
        Token char_literal48=null;
        LEACParser.typename_return typename45 = null;

        LEACParser.typename_return typename49 = null;


        Object IDF43_tree=null;
        Object char_literal44_tree=null;
        Object string_literal46_tree=null;
        Object IDF47_tree=null;
        Object char_literal48_tree=null;
        RewriteRuleTokenStream stream_IDF=new RewriteRuleTokenStream(adaptor,"token IDF");
        RewriteRuleTokenStream stream_23=new RewriteRuleTokenStream(adaptor,"token 23");
        RewriteRuleTokenStream stream_34=new RewriteRuleTokenStream(adaptor,"token 34");
        RewriteRuleSubtreeStream stream_typename=new RewriteRuleSubtreeStream(adaptor,"rule typename");
        try {
            // src/LEAC.g:28:5: ( IDF ':' typename -> ^( ARG IDF typename ) | 'ref' IDF ':' typename -> ^( ARGREF IDF typename ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==IDF) ) {
                alt12=1;
            }
            else if ( (LA12_0==34) ) {
                alt12=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // src/LEAC.g:28:7: IDF ':' typename
                    {
                    IDF43=(Token)match(input,IDF,FOLLOW_IDF_in_arg297); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_IDF.add(IDF43);

                    char_literal44=(Token)match(input,23,FOLLOW_23_in_arg299); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_23.add(char_literal44);

                    pushFollow(FOLLOW_typename_in_arg301);
                    typename45=typename();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) stream_typename.add(typename45.getTree());


                    // AST REWRITE
                    // elements: typename, IDF
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if ( state.backtracking==0 ) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 28:24: -> ^( ARG IDF typename )
                    {
                        // src/LEAC.g:28:27: ^( ARG IDF typename )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ARG, "ARG"), root_1);

                        adaptor.addChild(root_1, stream_IDF.nextNode());
                        adaptor.addChild(root_1, stream_typename.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;}
                    }
                    break;
                case 2 :
                    // src/LEAC.g:29:7: 'ref' IDF ':' typename
                    {
                    string_literal46=(Token)match(input,34,FOLLOW_34_in_arg319); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_34.add(string_literal46);

                    IDF47=(Token)match(input,IDF,FOLLOW_IDF_in_arg321); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_IDF.add(IDF47);

                    char_literal48=(Token)match(input,23,FOLLOW_23_in_arg323); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_23.add(char_literal48);

                    pushFollow(FOLLOW_typename_in_arg325);
                    typename49=typename();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) stream_typename.add(typename49.getTree());


                    // AST REWRITE
                    // elements: IDF, typename
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if ( state.backtracking==0 ) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 29:30: -> ^( ARGREF IDF typename )
                    {
                        // src/LEAC.g:29:33: ^( ARGREF IDF typename )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ARGREF, "ARGREF"), root_1);

                        adaptor.addChild(root_1, stream_IDF.nextNode());
                        adaptor.addChild(root_1, stream_typename.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;}
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "arg"

    public static class instr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "instr"
    // src/LEAC.g:31:1: instr : ( 'if' expr 'then' instr ( 'else' instr )? | 'while' expr 'do' instr | lvalue '=' expr | 'return' ( expr )? | IDF '(' ( exprlist )? ')' -> ^( FUNCTION ^( IDF ( exprlist )? ) ) | 'read' lvalue | 'write' ( lvalue | cste ) );
    public final LEACParser.instr_return instr() throws RecognitionException {
        LEACParser.instr_return retval = new LEACParser.instr_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal50=null;
        Token string_literal52=null;
        Token string_literal54=null;
        Token string_literal56=null;
        Token string_literal58=null;
        Token char_literal61=null;
        Token string_literal63=null;
        Token IDF65=null;
        Token char_literal66=null;
        Token char_literal68=null;
        Token string_literal69=null;
        Token string_literal71=null;
        LEACParser.expr_return expr51 = null;

        LEACParser.instr_return instr53 = null;

        LEACParser.instr_return instr55 = null;

        LEACParser.expr_return expr57 = null;

        LEACParser.instr_return instr59 = null;

        LEACParser.lvalue_return lvalue60 = null;

        LEACParser.expr_return expr62 = null;

        LEACParser.expr_return expr64 = null;

        LEACParser.exprlist_return exprlist67 = null;

        LEACParser.lvalue_return lvalue70 = null;

        LEACParser.lvalue_return lvalue72 = null;

        LEACParser.cste_return cste73 = null;


        Object string_literal50_tree=null;
        Object string_literal52_tree=null;
        Object string_literal54_tree=null;
        Object string_literal56_tree=null;
        Object string_literal58_tree=null;
        Object char_literal61_tree=null;
        Object string_literal63_tree=null;
        Object IDF65_tree=null;
        Object char_literal66_tree=null;
        Object char_literal68_tree=null;
        Object string_literal69_tree=null;
        Object string_literal71_tree=null;
        RewriteRuleTokenStream stream_32=new RewriteRuleTokenStream(adaptor,"token 32");
        RewriteRuleTokenStream stream_IDF=new RewriteRuleTokenStream(adaptor,"token IDF");
        RewriteRuleTokenStream stream_33=new RewriteRuleTokenStream(adaptor,"token 33");
        RewriteRuleSubtreeStream stream_exprlist=new RewriteRuleSubtreeStream(adaptor,"rule exprlist");
        try {
            // src/LEAC.g:31:7: ( 'if' expr 'then' instr ( 'else' instr )? | 'while' expr 'do' instr | lvalue '=' expr | 'return' ( expr )? | IDF '(' ( exprlist )? ')' -> ^( FUNCTION ^( IDF ( exprlist )? ) ) | 'read' lvalue | 'write' ( lvalue | cste ) )
            int alt17=7;
            switch ( input.LA(1) ) {
            case 35:
                {
                alt17=1;
                }
                break;
            case 38:
                {
                alt17=2;
                }
                break;
            case IDF:
                {
                int LA17_3 = input.LA(2);

                if ( (LA17_3==32) ) {
                    alt17=5;
                }
                else if ( (LA17_3==27||LA17_3==40) ) {
                    alt17=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 17, 3, input);

                    throw nvae;
                }
                }
                break;
            case 41:
                {
                alt17=4;
                }
                break;
            case 42:
                {
                alt17=6;
                }
                break;
            case 43:
                {
                alt17=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // src/LEAC.g:31:9: 'if' expr 'then' instr ( 'else' instr )?
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal50=(Token)match(input,35,FOLLOW_35_in_instr343); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal50_tree = (Object)adaptor.create(string_literal50);
                    root_0 = (Object)adaptor.becomeRoot(string_literal50_tree, root_0);
                    }
                    pushFollow(FOLLOW_expr_in_instr346);
                    expr51=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expr51.getTree());
                    string_literal52=(Token)match(input,36,FOLLOW_36_in_instr348); if (state.failed) return retval;
                    pushFollow(FOLLOW_instr_in_instr351);
                    instr53=instr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, instr53.getTree());
                    // src/LEAC.g:31:34: ( 'else' instr )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==37) ) {
                        int LA13_1 = input.LA(2);

                        if ( (synpred13_LEAC()) ) {
                            alt13=1;
                        }
                    }
                    switch (alt13) {
                        case 1 :
                            // src/LEAC.g:31:35: 'else' instr
                            {
                            string_literal54=(Token)match(input,37,FOLLOW_37_in_instr354); if (state.failed) return retval;
                            pushFollow(FOLLOW_instr_in_instr357);
                            instr55=instr();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, instr55.getTree());

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // src/LEAC.g:32:9: 'while' expr 'do' instr
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal56=(Token)match(input,38,FOLLOW_38_in_instr369); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal56_tree = (Object)adaptor.create(string_literal56);
                    root_0 = (Object)adaptor.becomeRoot(string_literal56_tree, root_0);
                    }
                    pushFollow(FOLLOW_expr_in_instr372);
                    expr57=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expr57.getTree());
                    string_literal58=(Token)match(input,39,FOLLOW_39_in_instr374); if (state.failed) return retval;
                    pushFollow(FOLLOW_instr_in_instr377);
                    instr59=instr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, instr59.getTree());

                    }
                    break;
                case 3 :
                    // src/LEAC.g:33:9: lvalue '=' expr
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_lvalue_in_instr387);
                    lvalue60=lvalue();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, lvalue60.getTree());
                    char_literal61=(Token)match(input,40,FOLLOW_40_in_instr389); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal61_tree = (Object)adaptor.create(char_literal61);
                    root_0 = (Object)adaptor.becomeRoot(char_literal61_tree, root_0);
                    }
                    pushFollow(FOLLOW_expr_in_instr392);
                    expr62=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expr62.getTree());

                    }
                    break;
                case 4 :
                    // src/LEAC.g:34:9: 'return' ( expr )?
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal63=(Token)match(input,41,FOLLOW_41_in_instr402); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal63_tree = (Object)adaptor.create(string_literal63);
                    root_0 = (Object)adaptor.becomeRoot(string_literal63_tree, root_0);
                    }
                    // src/LEAC.g:34:19: ( expr )?
                    int alt14=2;
                    alt14 = dfa14.predict(input);
                    switch (alt14) {
                        case 1 :
                            // src/LEAC.g:0:0: expr
                            {
                            pushFollow(FOLLOW_expr_in_instr405);
                            expr64=expr();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, expr64.getTree());

                            }
                            break;

                    }


                    }
                    break;
                case 5 :
                    // src/LEAC.g:35:9: IDF '(' ( exprlist )? ')'
                    {
                    IDF65=(Token)match(input,IDF,FOLLOW_IDF_in_instr416); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_IDF.add(IDF65);

                    char_literal66=(Token)match(input,32,FOLLOW_32_in_instr418); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_32.add(char_literal66);

                    // src/LEAC.g:35:17: ( exprlist )?
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( (LA15_0==IDF||(LA15_0>=INT && LA15_0<=TXT)||LA15_0==32||LA15_0==55||(LA15_0>=59 && LA15_0<=61)) ) {
                        alt15=1;
                    }
                    switch (alt15) {
                        case 1 :
                            // src/LEAC.g:0:0: exprlist
                            {
                            pushFollow(FOLLOW_exprlist_in_instr420);
                            exprlist67=exprlist();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) stream_exprlist.add(exprlist67.getTree());

                            }
                            break;

                    }

                    char_literal68=(Token)match(input,33,FOLLOW_33_in_instr423); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_33.add(char_literal68);



                    // AST REWRITE
                    // elements: IDF, exprlist
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if ( state.backtracking==0 ) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 35:31: -> ^( FUNCTION ^( IDF ( exprlist )? ) )
                    {
                        // src/LEAC.g:35:34: ^( FUNCTION ^( IDF ( exprlist )? ) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(FUNCTION, "FUNCTION"), root_1);

                        // src/LEAC.g:35:45: ^( IDF ( exprlist )? )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot(stream_IDF.nextNode(), root_2);

                        // src/LEAC.g:35:51: ( exprlist )?
                        if ( stream_exprlist.hasNext() ) {
                            adaptor.addChild(root_2, stream_exprlist.nextTree());

                        }
                        stream_exprlist.reset();

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;}
                    }
                    break;
                case 6 :
                    // src/LEAC.g:36:9: 'read' lvalue
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal69=(Token)match(input,42,FOLLOW_42_in_instr446); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal69_tree = (Object)adaptor.create(string_literal69);
                    root_0 = (Object)adaptor.becomeRoot(string_literal69_tree, root_0);
                    }
                    pushFollow(FOLLOW_lvalue_in_instr449);
                    lvalue70=lvalue();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, lvalue70.getTree());

                    }
                    break;
                case 7 :
                    // src/LEAC.g:37:9: 'write' ( lvalue | cste )
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal71=(Token)match(input,43,FOLLOW_43_in_instr459); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal71_tree = (Object)adaptor.create(string_literal71);
                    root_0 = (Object)adaptor.becomeRoot(string_literal71_tree, root_0);
                    }
                    // src/LEAC.g:37:18: ( lvalue | cste )
                    int alt16=2;
                    int LA16_0 = input.LA(1);

                    if ( (LA16_0==IDF) ) {
                        alt16=1;
                    }
                    else if ( ((LA16_0>=INT && LA16_0<=TXT)||(LA16_0>=60 && LA16_0<=61)) ) {
                        alt16=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return retval;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 16, 0, input);

                        throw nvae;
                    }
                    switch (alt16) {
                        case 1 :
                            // src/LEAC.g:37:19: lvalue
                            {
                            pushFollow(FOLLOW_lvalue_in_instr463);
                            lvalue72=lvalue();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, lvalue72.getTree());

                            }
                            break;
                        case 2 :
                            // src/LEAC.g:37:26: cste
                            {
                            pushFollow(FOLLOW_cste_in_instr465);
                            cste73=cste();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, cste73.getTree());

                            }
                            break;

                    }


                    }
                    break;

            }
            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "instr"

    public static class instrsp_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "instrsp"
    // src/LEAC.g:39:1: instrsp : ( 'if' expr 'then' '{' ( block )? '}' ( 'else' '{' ( block )? '}' )? | 'while' expr 'do' '{' ( block )? '}' | '{' ( block )? '}' );
    public final LEACParser.instrsp_return instrsp() throws RecognitionException {
        LEACParser.instrsp_return retval = new LEACParser.instrsp_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal74=null;
        Token string_literal76=null;
        Token char_literal77=null;
        Token char_literal79=null;
        Token string_literal80=null;
        Token char_literal81=null;
        Token char_literal83=null;
        Token string_literal84=null;
        Token string_literal86=null;
        Token char_literal87=null;
        Token char_literal89=null;
        Token char_literal90=null;
        Token char_literal92=null;
        LEACParser.expr_return expr75 = null;

        LEACParser.block_return block78 = null;

        LEACParser.block_return block82 = null;

        LEACParser.expr_return expr85 = null;

        LEACParser.block_return block88 = null;

        LEACParser.block_return block91 = null;


        Object string_literal74_tree=null;
        Object string_literal76_tree=null;
        Object char_literal77_tree=null;
        Object char_literal79_tree=null;
        Object string_literal80_tree=null;
        Object char_literal81_tree=null;
        Object char_literal83_tree=null;
        Object string_literal84_tree=null;
        Object string_literal86_tree=null;
        Object char_literal87_tree=null;
        Object char_literal89_tree=null;
        Object char_literal90_tree=null;
        Object char_literal92_tree=null;

        try {
            // src/LEAC.g:39:9: ( 'if' expr 'then' '{' ( block )? '}' ( 'else' '{' ( block )? '}' )? | 'while' expr 'do' '{' ( block )? '}' | '{' ( block )? '}' )
            int alt23=3;
            switch ( input.LA(1) ) {
            case 35:
                {
                alt23=1;
                }
                break;
            case 38:
                {
                alt23=2;
                }
                break;
            case 44:
                {
                alt23=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // src/LEAC.g:39:11: 'if' expr 'then' '{' ( block )? '}' ( 'else' '{' ( block )? '}' )?
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal74=(Token)match(input,35,FOLLOW_35_in_instrsp474); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal74_tree = (Object)adaptor.create(string_literal74);
                    root_0 = (Object)adaptor.becomeRoot(string_literal74_tree, root_0);
                    }
                    pushFollow(FOLLOW_expr_in_instrsp477);
                    expr75=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expr75.getTree());
                    string_literal76=(Token)match(input,36,FOLLOW_36_in_instrsp479); if (state.failed) return retval;
                    char_literal77=(Token)match(input,44,FOLLOW_44_in_instrsp482); if (state.failed) return retval;
                    // src/LEAC.g:39:35: ( block )?
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==IDF||LA18_0==35||LA18_0==38||(LA18_0>=41 && LA18_0<=44)) ) {
                        alt18=1;
                    }
                    switch (alt18) {
                        case 1 :
                            // src/LEAC.g:0:0: block
                            {
                            pushFollow(FOLLOW_block_in_instrsp485);
                            block78=block();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, block78.getTree());

                            }
                            break;

                    }

                    char_literal79=(Token)match(input,45,FOLLOW_45_in_instrsp488); if (state.failed) return retval;
                    // src/LEAC.g:39:47: ( 'else' '{' ( block )? '}' )?
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0==37) ) {
                        alt20=1;
                    }
                    switch (alt20) {
                        case 1 :
                            // src/LEAC.g:39:48: 'else' '{' ( block )? '}'
                            {
                            string_literal80=(Token)match(input,37,FOLLOW_37_in_instrsp492); if (state.failed) return retval;
                            char_literal81=(Token)match(input,44,FOLLOW_44_in_instrsp495); if (state.failed) return retval;
                            // src/LEAC.g:39:61: ( block )?
                            int alt19=2;
                            int LA19_0 = input.LA(1);

                            if ( (LA19_0==IDF||LA19_0==35||LA19_0==38||(LA19_0>=41 && LA19_0<=44)) ) {
                                alt19=1;
                            }
                            switch (alt19) {
                                case 1 :
                                    // src/LEAC.g:0:0: block
                                    {
                                    pushFollow(FOLLOW_block_in_instrsp498);
                                    block82=block();

                                    state._fsp--;
                                    if (state.failed) return retval;
                                    if ( state.backtracking==0 ) adaptor.addChild(root_0, block82.getTree());

                                    }
                                    break;

                            }

                            char_literal83=(Token)match(input,45,FOLLOW_45_in_instrsp501); if (state.failed) return retval;

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // src/LEAC.g:40:11: 'while' expr 'do' '{' ( block )? '}'
                    {
                    root_0 = (Object)adaptor.nil();

                    string_literal84=(Token)match(input,38,FOLLOW_38_in_instrsp516); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal84_tree = (Object)adaptor.create(string_literal84);
                    root_0 = (Object)adaptor.becomeRoot(string_literal84_tree, root_0);
                    }
                    pushFollow(FOLLOW_expr_in_instrsp519);
                    expr85=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expr85.getTree());
                    string_literal86=(Token)match(input,39,FOLLOW_39_in_instrsp521); if (state.failed) return retval;
                    char_literal87=(Token)match(input,44,FOLLOW_44_in_instrsp524); if (state.failed) return retval;
                    // src/LEAC.g:40:36: ( block )?
                    int alt21=2;
                    int LA21_0 = input.LA(1);

                    if ( (LA21_0==IDF||LA21_0==35||LA21_0==38||(LA21_0>=41 && LA21_0<=44)) ) {
                        alt21=1;
                    }
                    switch (alt21) {
                        case 1 :
                            // src/LEAC.g:0:0: block
                            {
                            pushFollow(FOLLOW_block_in_instrsp527);
                            block88=block();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, block88.getTree());

                            }
                            break;

                    }

                    char_literal89=(Token)match(input,45,FOLLOW_45_in_instrsp530); if (state.failed) return retval;

                    }
                    break;
                case 3 :
                    // src/LEAC.g:41:11: '{' ( block )? '}'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal90=(Token)match(input,44,FOLLOW_44_in_instrsp543); if (state.failed) return retval;
                    // src/LEAC.g:41:16: ( block )?
                    int alt22=2;
                    int LA22_0 = input.LA(1);

                    if ( (LA22_0==IDF||LA22_0==35||LA22_0==38||(LA22_0>=41 && LA22_0<=44)) ) {
                        alt22=1;
                    }
                    switch (alt22) {
                        case 1 :
                            // src/LEAC.g:0:0: block
                            {
                            pushFollow(FOLLOW_block_in_instrsp546);
                            block91=block();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, block91.getTree());

                            }
                            break;

                    }

                    char_literal92=(Token)match(input,45,FOLLOW_45_in_instrsp549); if (state.failed) return retval;

                    }
                    break;

            }
            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "instrsp"

    public static class block_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "block"
    // src/LEAC.g:43:1: block : sequence -> ^( SEQUENCE sequence ) ;
    public final LEACParser.block_return block() throws RecognitionException {
        LEACParser.block_return retval = new LEACParser.block_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        LEACParser.sequence_return sequence93 = null;


        RewriteRuleSubtreeStream stream_sequence=new RewriteRuleSubtreeStream(adaptor,"rule sequence");
        try {
            // src/LEAC.g:43:6: ( sequence -> ^( SEQUENCE sequence ) )
            // src/LEAC.g:43:8: sequence
            {
            pushFollow(FOLLOW_sequence_in_block557);
            sequence93=sequence();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) stream_sequence.add(sequence93.getTree());


            // AST REWRITE
            // elements: sequence
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            if ( state.backtracking==0 ) {
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 43:17: -> ^( SEQUENCE sequence )
            {
                // src/LEAC.g:43:20: ^( SEQUENCE sequence )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SEQUENCE, "SEQUENCE"), root_1);

                adaptor.addChild(root_1, stream_sequence.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;}
            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "block"

    public static class sequence_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "sequence"
    // src/LEAC.g:45:1: sequence : ( instr ';' | instrsp )* ( instr | instrsp ) ;
    public final LEACParser.sequence_return sequence() throws RecognitionException {
        LEACParser.sequence_return retval = new LEACParser.sequence_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal95=null;
        LEACParser.instr_return instr94 = null;

        LEACParser.instrsp_return instrsp96 = null;

        LEACParser.instr_return instr97 = null;

        LEACParser.instrsp_return instrsp98 = null;


        Object char_literal95_tree=null;

        try {
            // src/LEAC.g:45:10: ( ( instr ';' | instrsp )* ( instr | instrsp ) )
            // src/LEAC.g:45:12: ( instr ';' | instrsp )* ( instr | instrsp )
            {
            root_0 = (Object)adaptor.nil();

            // src/LEAC.g:45:12: ( instr ';' | instrsp )*
            loop24:
            do {
                int alt24=3;
                alt24 = dfa24.predict(input);
                switch (alt24) {
            	case 1 :
            	    // src/LEAC.g:45:13: instr ';'
            	    {
            	    pushFollow(FOLLOW_instr_in_sequence574);
            	    instr94=instr();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, instr94.getTree());
            	    char_literal95=(Token)match(input,24,FOLLOW_24_in_sequence576); if (state.failed) return retval;

            	    }
            	    break;
            	case 2 :
            	    // src/LEAC.g:45:24: instrsp
            	    {
            	    pushFollow(FOLLOW_instrsp_in_sequence579);
            	    instrsp96=instrsp();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, instrsp96.getTree());

            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            // src/LEAC.g:45:34: ( instr | instrsp )
            int alt25=2;
            switch ( input.LA(1) ) {
            case 35:
                {
                int LA25_1 = input.LA(2);

                if ( (synpred32_LEAC()) ) {
                    alt25=1;
                }
                else if ( (true) ) {
                    alt25=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 25, 1, input);

                    throw nvae;
                }
                }
                break;
            case 38:
                {
                int LA25_2 = input.LA(2);

                if ( (synpred32_LEAC()) ) {
                    alt25=1;
                }
                else if ( (true) ) {
                    alt25=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 25, 2, input);

                    throw nvae;
                }
                }
                break;
            case IDF:
            case 41:
            case 42:
            case 43:
                {
                alt25=1;
                }
                break;
            case 44:
                {
                alt25=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }

            switch (alt25) {
                case 1 :
                    // src/LEAC.g:45:35: instr
                    {
                    pushFollow(FOLLOW_instr_in_sequence584);
                    instr97=instr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, instr97.getTree());

                    }
                    break;
                case 2 :
                    // src/LEAC.g:45:41: instrsp
                    {
                    pushFollow(FOLLOW_instrsp_in_sequence586);
                    instrsp98=instrsp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, instrsp98.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "sequence"

    public static class lvalue_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "lvalue"
    // src/LEAC.g:47:1: lvalue : IDF ( '[' exprlist ']' )? ;
    public final LEACParser.lvalue_return lvalue() throws RecognitionException {
        LEACParser.lvalue_return retval = new LEACParser.lvalue_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token IDF99=null;
        Token char_literal100=null;
        Token char_literal102=null;
        LEACParser.exprlist_return exprlist101 = null;


        Object IDF99_tree=null;
        Object char_literal100_tree=null;
        Object char_literal102_tree=null;

        try {
            // src/LEAC.g:47:8: ( IDF ( '[' exprlist ']' )? )
            // src/LEAC.g:47:10: IDF ( '[' exprlist ']' )?
            {
            root_0 = (Object)adaptor.nil();

            IDF99=(Token)match(input,IDF,FOLLOW_IDF_in_lvalue595); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDF99_tree = (Object)adaptor.create(IDF99);
            root_0 = (Object)adaptor.becomeRoot(IDF99_tree, root_0);
            }
            // src/LEAC.g:47:15: ( '[' exprlist ']' )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==27) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // src/LEAC.g:47:16: '[' exprlist ']'
                    {
                    char_literal100=(Token)match(input,27,FOLLOW_27_in_lvalue599); if (state.failed) return retval;
                    pushFollow(FOLLOW_exprlist_in_lvalue602);
                    exprlist101=exprlist();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, exprlist101.getTree());
                    char_literal102=(Token)match(input,28,FOLLOW_28_in_lvalue604); if (state.failed) return retval;

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "lvalue"

    public static class exprlist_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "exprlist"
    // src/LEAC.g:49:1: exprlist : expr ( ',' expr )* ;
    public final LEACParser.exprlist_return exprlist() throws RecognitionException {
        LEACParser.exprlist_return retval = new LEACParser.exprlist_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal104=null;
        LEACParser.expr_return expr103 = null;

        LEACParser.expr_return expr105 = null;


        Object char_literal104_tree=null;

        try {
            // src/LEAC.g:49:10: ( expr ( ',' expr )* )
            // src/LEAC.g:49:12: expr ( ',' expr )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_expr_in_exprlist615);
            expr103=expr();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, expr103.getTree());
            // src/LEAC.g:49:17: ( ',' expr )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==25) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // src/LEAC.g:49:18: ',' expr
            	    {
            	    char_literal104=(Token)match(input,25,FOLLOW_25_in_exprlist618); if (state.failed) return retval;
            	    pushFollow(FOLLOW_expr_in_exprlist621);
            	    expr105=expr();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, expr105.getTree());

            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "exprlist"

    public static class expr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "expr"
    // src/LEAC.g:51:1: expr : and ( 'or' and )* ;
    public final LEACParser.expr_return expr() throws RecognitionException {
        LEACParser.expr_return retval = new LEACParser.expr_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal107=null;
        LEACParser.and_return and106 = null;

        LEACParser.and_return and108 = null;


        Object string_literal107_tree=null;

        try {
            // src/LEAC.g:51:6: ( and ( 'or' and )* )
            // src/LEAC.g:51:8: and ( 'or' and )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_and_in_expr631);
            and106=and();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, and106.getTree());
            // src/LEAC.g:51:12: ( 'or' and )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==46) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // src/LEAC.g:51:13: 'or' and
            	    {
            	    string_literal107=(Token)match(input,46,FOLLOW_46_in_expr634); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    string_literal107_tree = (Object)adaptor.create(string_literal107);
            	    root_0 = (Object)adaptor.becomeRoot(string_literal107_tree, root_0);
            	    }
            	    pushFollow(FOLLOW_and_in_expr637);
            	    and108=and();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, and108.getTree());

            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "expr"

    public static class and_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "and"
    // src/LEAC.g:52:1: and : test ( 'and' test )* ;
    public final LEACParser.and_return and() throws RecognitionException {
        LEACParser.and_return retval = new LEACParser.and_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token string_literal110=null;
        LEACParser.test_return test109 = null;

        LEACParser.test_return test111 = null;


        Object string_literal110_tree=null;

        try {
            // src/LEAC.g:52:5: ( test ( 'and' test )* )
            // src/LEAC.g:52:7: test ( 'and' test )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_test_in_and646);
            test109=test();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, test109.getTree());
            // src/LEAC.g:52:12: ( 'and' test )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==47) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // src/LEAC.g:52:13: 'and' test
            	    {
            	    string_literal110=(Token)match(input,47,FOLLOW_47_in_and649); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    string_literal110_tree = (Object)adaptor.create(string_literal110);
            	    root_0 = (Object)adaptor.becomeRoot(string_literal110_tree, root_0);
            	    }
            	    pushFollow(FOLLOW_test_in_and652);
            	    test111=test();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, test111.getTree());

            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "and"

    public static class test_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "test"
    // src/LEAC.g:53:1: test : add ( ( '<' | '<=' | '>' | '>=' | '==' | '!=' ) add )? ;
    public final LEACParser.test_return test() throws RecognitionException {
        LEACParser.test_return retval = new LEACParser.test_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set113=null;
        LEACParser.add_return add112 = null;

        LEACParser.add_return add114 = null;


        Object set113_tree=null;

        try {
            // src/LEAC.g:53:6: ( add ( ( '<' | '<=' | '>' | '>=' | '==' | '!=' ) add )? )
            // src/LEAC.g:53:8: add ( ( '<' | '<=' | '>' | '>=' | '==' | '!=' ) add )?
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_add_in_test661);
            add112=add();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, add112.getTree());
            // src/LEAC.g:53:12: ( ( '<' | '<=' | '>' | '>=' | '==' | '!=' ) add )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( ((LA30_0>=48 && LA30_0<=53)) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // src/LEAC.g:53:13: ( '<' | '<=' | '>' | '>=' | '==' | '!=' ) add
                    {
                    set113=(Token)input.LT(1);
                    set113=(Token)input.LT(1);
                    if ( (input.LA(1)>=48 && input.LA(1)<=53) ) {
                        input.consume();
                        if ( state.backtracking==0 ) root_0 = (Object)adaptor.becomeRoot((Object)adaptor.create(set113), root_0);
                        state.errorRecovery=false;state.failed=false;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return retval;}
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }

                    pushFollow(FOLLOW_add_in_test689);
                    add114=add();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, add114.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "test"

    public static class add_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "add"
    // src/LEAC.g:54:1: add : mult ( ( '+' | '-' ) mult )* ;
    public final LEACParser.add_return add() throws RecognitionException {
        LEACParser.add_return retval = new LEACParser.add_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set116=null;
        LEACParser.mult_return mult115 = null;

        LEACParser.mult_return mult117 = null;


        Object set116_tree=null;

        try {
            // src/LEAC.g:54:4: ( mult ( ( '+' | '-' ) mult )* )
            // src/LEAC.g:54:6: mult ( ( '+' | '-' ) mult )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_mult_in_add697);
            mult115=mult();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, mult115.getTree());
            // src/LEAC.g:54:11: ( ( '+' | '-' ) mult )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( ((LA31_0>=54 && LA31_0<=55)) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // src/LEAC.g:54:12: ( '+' | '-' ) mult
            	    {
            	    set116=(Token)input.LT(1);
            	    set116=(Token)input.LT(1);
            	    if ( (input.LA(1)>=54 && input.LA(1)<=55) ) {
            	        input.consume();
            	        if ( state.backtracking==0 ) root_0 = (Object)adaptor.becomeRoot((Object)adaptor.create(set116), root_0);
            	        state.errorRecovery=false;state.failed=false;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }

            	    pushFollow(FOLLOW_mult_in_add707);
            	    mult117=mult();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, mult117.getTree());

            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "add"

    public static class mult_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "mult"
    // src/LEAC.g:55:1: mult : pow ( ( '*' | '/' ) pow )* ;
    public final LEACParser.mult_return mult() throws RecognitionException {
        LEACParser.mult_return retval = new LEACParser.mult_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set119=null;
        LEACParser.pow_return pow118 = null;

        LEACParser.pow_return pow120 = null;


        Object set119_tree=null;

        try {
            // src/LEAC.g:55:5: ( pow ( ( '*' | '/' ) pow )* )
            // src/LEAC.g:55:7: pow ( ( '*' | '/' ) pow )*
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_pow_in_mult715);
            pow118=pow();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, pow118.getTree());
            // src/LEAC.g:55:11: ( ( '*' | '/' ) pow )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( ((LA32_0>=56 && LA32_0<=57)) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // src/LEAC.g:55:12: ( '*' | '/' ) pow
            	    {
            	    set119=(Token)input.LT(1);
            	    set119=(Token)input.LT(1);
            	    if ( (input.LA(1)>=56 && input.LA(1)<=57) ) {
            	        input.consume();
            	        if ( state.backtracking==0 ) root_0 = (Object)adaptor.becomeRoot((Object)adaptor.create(set119), root_0);
            	        state.errorRecovery=false;state.failed=false;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }

            	    pushFollow(FOLLOW_pow_in_mult725);
            	    pow120=pow();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, pow120.getTree());

            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "mult"

    public static class pow_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "pow"
    // src/LEAC.g:56:1: pow : neg ( '^' pow )? ;
    public final LEACParser.pow_return pow() throws RecognitionException {
        LEACParser.pow_return retval = new LEACParser.pow_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal122=null;
        LEACParser.neg_return neg121 = null;

        LEACParser.pow_return pow123 = null;


        Object char_literal122_tree=null;

        try {
            // src/LEAC.g:56:5: ( neg ( '^' pow )? )
            // src/LEAC.g:56:7: neg ( '^' pow )?
            {
            root_0 = (Object)adaptor.nil();

            pushFollow(FOLLOW_neg_in_pow734);
            neg121=neg();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, neg121.getTree());
            // src/LEAC.g:56:11: ( '^' pow )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==58) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // src/LEAC.g:56:12: '^' pow
                    {
                    char_literal122=(Token)match(input,58,FOLLOW_58_in_pow737); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal122_tree = (Object)adaptor.create(char_literal122);
                    root_0 = (Object)adaptor.becomeRoot(char_literal122_tree, root_0);
                    }
                    pushFollow(FOLLOW_pow_in_pow740);
                    pow123=pow();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, pow123.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "pow"

    public static class neg_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "neg"
    // src/LEAC.g:57:1: neg : ( '-' | 'not' )? atom ;
    public final LEACParser.neg_return neg() throws RecognitionException {
        LEACParser.neg_return retval = new LEACParser.neg_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token char_literal124=null;
        Token string_literal125=null;
        LEACParser.atom_return atom126 = null;


        Object char_literal124_tree=null;
        Object string_literal125_tree=null;

        try {
            // src/LEAC.g:57:5: ( ( '-' | 'not' )? atom )
            // src/LEAC.g:57:7: ( '-' | 'not' )? atom
            {
            root_0 = (Object)adaptor.nil();

            // src/LEAC.g:57:7: ( '-' | 'not' )?
            int alt34=3;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==55) ) {
                alt34=1;
            }
            else if ( (LA34_0==59) ) {
                alt34=2;
            }
            switch (alt34) {
                case 1 :
                    // src/LEAC.g:57:8: '-'
                    {
                    char_literal124=(Token)match(input,55,FOLLOW_55_in_neg750); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal124_tree = (Object)adaptor.create(char_literal124);
                    root_0 = (Object)adaptor.becomeRoot(char_literal124_tree, root_0);
                    }

                    }
                    break;
                case 2 :
                    // src/LEAC.g:57:13: 'not'
                    {
                    string_literal125=(Token)match(input,59,FOLLOW_59_in_neg753); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal125_tree = (Object)adaptor.create(string_literal125);
                    root_0 = (Object)adaptor.becomeRoot(string_literal125_tree, root_0);
                    }

                    }
                    break;

            }

            pushFollow(FOLLOW_atom_in_neg758);
            atom126=atom();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, atom126.getTree());

            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "neg"

    public static class atom_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "atom"
    // src/LEAC.g:58:1: atom : ( cste | ( IDF ( '(' ( exprlist )? ')' | '[' exprlist ']' )? ) | '(' expr ')' );
    public final LEACParser.atom_return atom() throws RecognitionException {
        LEACParser.atom_return retval = new LEACParser.atom_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token IDF128=null;
        Token char_literal129=null;
        Token char_literal131=null;
        Token char_literal132=null;
        Token char_literal134=null;
        Token char_literal135=null;
        Token char_literal137=null;
        LEACParser.cste_return cste127 = null;

        LEACParser.exprlist_return exprlist130 = null;

        LEACParser.exprlist_return exprlist133 = null;

        LEACParser.expr_return expr136 = null;


        Object IDF128_tree=null;
        Object char_literal129_tree=null;
        Object char_literal131_tree=null;
        Object char_literal132_tree=null;
        Object char_literal134_tree=null;
        Object char_literal135_tree=null;
        Object char_literal137_tree=null;

        try {
            // src/LEAC.g:58:6: ( cste | ( IDF ( '(' ( exprlist )? ')' | '[' exprlist ']' )? ) | '(' expr ')' )
            int alt37=3;
            switch ( input.LA(1) ) {
            case INT:
            case TXT:
            case 60:
            case 61:
                {
                alt37=1;
                }
                break;
            case IDF:
                {
                alt37=2;
                }
                break;
            case 32:
                {
                alt37=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;
            }

            switch (alt37) {
                case 1 :
                    // src/LEAC.g:58:8: cste
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_cste_in_atom765);
                    cste127=cste();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, cste127.getTree());

                    }
                    break;
                case 2 :
                    // src/LEAC.g:58:15: ( IDF ( '(' ( exprlist )? ')' | '[' exprlist ']' )? )
                    {
                    root_0 = (Object)adaptor.nil();

                    // src/LEAC.g:58:15: ( IDF ( '(' ( exprlist )? ')' | '[' exprlist ']' )? )
                    // src/LEAC.g:58:17: IDF ( '(' ( exprlist )? ')' | '[' exprlist ']' )?
                    {
                    IDF128=(Token)match(input,IDF,FOLLOW_IDF_in_atom771); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDF128_tree = (Object)adaptor.create(IDF128);
                    root_0 = (Object)adaptor.becomeRoot(IDF128_tree, root_0);
                    }
                    // src/LEAC.g:58:22: ( '(' ( exprlist )? ')' | '[' exprlist ']' )?
                    int alt36=3;
                    int LA36_0 = input.LA(1);

                    if ( (LA36_0==32) ) {
                        alt36=1;
                    }
                    else if ( (LA36_0==27) ) {
                        alt36=2;
                    }
                    switch (alt36) {
                        case 1 :
                            // src/LEAC.g:58:24: '(' ( exprlist )? ')'
                            {
                            char_literal129=(Token)match(input,32,FOLLOW_32_in_atom776); if (state.failed) return retval;
                            // src/LEAC.g:58:29: ( exprlist )?
                            int alt35=2;
                            int LA35_0 = input.LA(1);

                            if ( (LA35_0==IDF||(LA35_0>=INT && LA35_0<=TXT)||LA35_0==32||LA35_0==55||(LA35_0>=59 && LA35_0<=61)) ) {
                                alt35=1;
                            }
                            switch (alt35) {
                                case 1 :
                                    // src/LEAC.g:0:0: exprlist
                                    {
                                    pushFollow(FOLLOW_exprlist_in_atom779);
                                    exprlist130=exprlist();

                                    state._fsp--;
                                    if (state.failed) return retval;
                                    if ( state.backtracking==0 ) adaptor.addChild(root_0, exprlist130.getTree());

                                    }
                                    break;

                            }

                            char_literal131=(Token)match(input,33,FOLLOW_33_in_atom782); if (state.failed) return retval;

                            }
                            break;
                        case 2 :
                            // src/LEAC.g:58:46: '[' exprlist ']'
                            {
                            char_literal132=(Token)match(input,27,FOLLOW_27_in_atom787); if (state.failed) return retval;
                            pushFollow(FOLLOW_exprlist_in_atom790);
                            exprlist133=exprlist();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, exprlist133.getTree());
                            char_literal134=(Token)match(input,28,FOLLOW_28_in_atom792); if (state.failed) return retval;

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 3 :
                    // src/LEAC.g:58:72: '(' expr ')'
                    {
                    root_0 = (Object)adaptor.nil();

                    char_literal135=(Token)match(input,32,FOLLOW_32_in_atom802); if (state.failed) return retval;
                    pushFollow(FOLLOW_expr_in_atom805);
                    expr136=expr();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expr136.getTree());
                    char_literal137=(Token)match(input,33,FOLLOW_33_in_atom807); if (state.failed) return retval;

                    }
                    break;

            }
            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "atom"

    public static class cste_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "cste"
    // src/LEAC.g:63:1: cste : ( INT | 'true' | 'false' | TXT );
    public final LEACParser.cste_return cste() throws RecognitionException {
        LEACParser.cste_return retval = new LEACParser.cste_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set138=null;

        Object set138_tree=null;

        try {
            // src/LEAC.g:63:6: ( INT | 'true' | 'false' | TXT )
            // src/LEAC.g:
            {
            root_0 = (Object)adaptor.nil();

            set138=(Token)input.LT(1);
            if ( (input.LA(1)>=INT && input.LA(1)<=TXT)||(input.LA(1)>=60 && input.LA(1)<=61) ) {
                input.consume();
                if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set138));
                state.errorRecovery=false;state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "cste"

    // $ANTLR start synpred1_LEAC
    public final void synpred1_LEAC_fragment() throws RecognitionException {   
        // src/LEAC.g:6:48: ( instr )
        // src/LEAC.g:6:48: instr
        {
        pushFollow(FOLLOW_instr_in_synpred1_LEAC57);
        instr();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred1_LEAC

    // $ANTLR start synpred9_LEAC
    public final void synpred9_LEAC_fragment() throws RecognitionException {   
        // src/LEAC.g:24:71: ( instr )
        // src/LEAC.g:24:71: instr
        {
        pushFollow(FOLLOW_instr_in_synpred9_LEAC258);
        instr();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred9_LEAC

    // $ANTLR start synpred13_LEAC
    public final void synpred13_LEAC_fragment() throws RecognitionException {   
        // src/LEAC.g:31:35: ( 'else' instr )
        // src/LEAC.g:31:35: 'else' instr
        {
        match(input,37,FOLLOW_37_in_synpred13_LEAC354); if (state.failed) return ;
        pushFollow(FOLLOW_instr_in_synpred13_LEAC357);
        instr();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred13_LEAC

    // $ANTLR start synpred17_LEAC
    public final void synpred17_LEAC_fragment() throws RecognitionException {   
        // src/LEAC.g:34:19: ( expr )
        // src/LEAC.g:34:19: expr
        {
        pushFollow(FOLLOW_expr_in_synpred17_LEAC405);
        expr();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred17_LEAC

    // $ANTLR start synpred30_LEAC
    public final void synpred30_LEAC_fragment() throws RecognitionException {   
        // src/LEAC.g:45:13: ( instr ';' )
        // src/LEAC.g:45:13: instr ';'
        {
        pushFollow(FOLLOW_instr_in_synpred30_LEAC574);
        instr();

        state._fsp--;
        if (state.failed) return ;
        match(input,24,FOLLOW_24_in_synpred30_LEAC576); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred30_LEAC

    // $ANTLR start synpred31_LEAC
    public final void synpred31_LEAC_fragment() throws RecognitionException {   
        // src/LEAC.g:45:24: ( instrsp )
        // src/LEAC.g:45:24: instrsp
        {
        pushFollow(FOLLOW_instrsp_in_synpred31_LEAC579);
        instrsp();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred31_LEAC

    // $ANTLR start synpred32_LEAC
    public final void synpred32_LEAC_fragment() throws RecognitionException {   
        // src/LEAC.g:45:35: ( instr )
        // src/LEAC.g:45:35: instr
        {
        pushFollow(FOLLOW_instr_in_synpred32_LEAC584);
        instr();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred32_LEAC

    // Delegated rules

    public final boolean synpred32_LEAC() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred32_LEAC_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred9_LEAC() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred9_LEAC_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred17_LEAC() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred17_LEAC_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred31_LEAC() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred31_LEAC_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred1_LEAC() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_LEAC_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred30_LEAC() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred30_LEAC_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred13_LEAC() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred13_LEAC_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA14 dfa14 = new DFA14(this);
    protected DFA24 dfa24 = new DFA24(this);
    static final String DFA14_eotS =
        "\21\uffff";
    static final String DFA14_eofS =
        "\1\6\20\uffff";
    static final String DFA14_minS =
        "\1\16\3\uffff\1\0\14\uffff";
    static final String DFA14_maxS =
        "\1\75\3\uffff\1\0\14\uffff";
    static final String DFA14_acceptS =
        "\1\uffff\1\1\4\uffff\1\2\12\uffff";
    static final String DFA14_specialS =
        "\4\uffff\1\0\14\uffff}>";
    static final String[] DFA14_transitionS = {
            "\1\4\1\uffff\2\1\6\uffff\1\6\6\uffff\1\6\1\1\2\uffff\1\6\1"+
            "\uffff\2\6\2\uffff\5\6\11\uffff\1\1\3\uffff\3\1",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "34:19: ( expr )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA14_4 = input.LA(1);

                         
                        int index14_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred17_LEAC()) ) {s = 1;}

                        else if ( (true) ) {s = 6;}

                         
                        input.seek(index14_4);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 14, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA24_eotS =
        "\13\uffff";
    static final String DFA24_eofS =
        "\13\uffff";
    static final String DFA24_minS =
        "\1\16\7\0\3\uffff";
    static final String DFA24_maxS =
        "\1\54\7\0\3\uffff";
    static final String DFA24_acceptS =
        "\10\uffff\1\1\1\2\1\3";
    static final String DFA24_specialS =
        "\1\uffff\1\0\1\1\1\2\1\3\1\4\1\5\1\6\3\uffff}>";
    static final String[] DFA24_transitionS = {
            "\1\3\24\uffff\1\1\2\uffff\1\2\2\uffff\1\4\1\5\1\6\1\7",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            ""
    };

    static final short[] DFA24_eot = DFA.unpackEncodedString(DFA24_eotS);
    static final short[] DFA24_eof = DFA.unpackEncodedString(DFA24_eofS);
    static final char[] DFA24_min = DFA.unpackEncodedStringToUnsignedChars(DFA24_minS);
    static final char[] DFA24_max = DFA.unpackEncodedStringToUnsignedChars(DFA24_maxS);
    static final short[] DFA24_accept = DFA.unpackEncodedString(DFA24_acceptS);
    static final short[] DFA24_special = DFA.unpackEncodedString(DFA24_specialS);
    static final short[][] DFA24_transition;

    static {
        int numStates = DFA24_transitionS.length;
        DFA24_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA24_transition[i] = DFA.unpackEncodedString(DFA24_transitionS[i]);
        }
    }

    class DFA24 extends DFA {

        public DFA24(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 24;
            this.eot = DFA24_eot;
            this.eof = DFA24_eof;
            this.min = DFA24_min;
            this.max = DFA24_max;
            this.accept = DFA24_accept;
            this.special = DFA24_special;
            this.transition = DFA24_transition;
        }
        public String getDescription() {
            return "()* loopback of 45:12: ( instr ';' | instrsp )*";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA24_1 = input.LA(1);

                         
                        int index24_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred30_LEAC()) ) {s = 8;}

                        else if ( (synpred31_LEAC()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index24_1);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA24_2 = input.LA(1);

                         
                        int index24_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred30_LEAC()) ) {s = 8;}

                        else if ( (synpred31_LEAC()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index24_2);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA24_3 = input.LA(1);

                         
                        int index24_3 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred30_LEAC()) ) {s = 8;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index24_3);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA24_4 = input.LA(1);

                         
                        int index24_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred30_LEAC()) ) {s = 8;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index24_4);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA24_5 = input.LA(1);

                         
                        int index24_5 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred30_LEAC()) ) {s = 8;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index24_5);
                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA24_6 = input.LA(1);

                         
                        int index24_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred30_LEAC()) ) {s = 8;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index24_6);
                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA24_7 = input.LA(1);

                         
                        int index24_7 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred31_LEAC()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index24_7);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 24, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_21_in_program47 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_IDF_in_program50 = new BitSet(new long[]{0x00001E4880404000L});
    public static final BitSet FOLLOW_vardeclist_in_program52 = new BitSet(new long[]{0x00001E4880404000L});
    public static final BitSet FOLLOW_fundeclist_in_program54 = new BitSet(new long[]{0x00001E4880404000L});
    public static final BitSet FOLLOW_instr_in_program57 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_instrsp_in_program59 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varsuitdecl_in_vardeclist69 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_22_in_varsuitdecl89 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_identlist_in_varsuitdecl92 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_varsuitdecl94 = new BitSet(new long[]{0x0000000004008000L});
    public static final BitSet FOLLOW_typename_in_varsuitdecl97 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_varsuitdecl99 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDF_in_identlist108 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_25_in_identlist111 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_IDF_in_identlist113 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_ATOMTYPE_in_typename132 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arraytype_in_typename134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_arraytype157 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_27_in_arraytype159 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rangelist_in_arraytype161 = new BitSet(new long[]{0x0000000012000000L});
    public static final BitSet FOLLOW_25_in_arraytype164 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rangelist_in_arraytype166 = new BitSet(new long[]{0x0000000012000000L});
    public static final BitSet FOLLOW_28_in_arraytype170 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_arraytype172 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_ATOMTYPE_in_arraytype174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INT_in_rangelist192 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_rangelist194 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_INT_in_rangelist196 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_fundecl_in_fundeclist216 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_31_in_fundecl237 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_IDF_in_fundecl240 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_fundecl242 = new BitSet(new long[]{0x0000000600004000L});
    public static final BitSet FOLLOW_arglist_in_fundecl245 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_fundecl247 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_fundecl250 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_ATOMTYPE_in_fundecl253 = new BitSet(new long[]{0x00001E4880404000L});
    public static final BitSet FOLLOW_vardeclist_in_fundecl255 = new BitSet(new long[]{0x00001E4880404000L});
    public static final BitSet FOLLOW_instr_in_fundecl258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_instrsp_in_fundecl260 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arg_in_arglist271 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_25_in_arglist274 = new BitSet(new long[]{0x0000000400004000L});
    public static final BitSet FOLLOW_arg_in_arglist276 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_IDF_in_arg297 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_arg299 = new BitSet(new long[]{0x0000000004008000L});
    public static final BitSet FOLLOW_typename_in_arg301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_arg319 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_IDF_in_arg321 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_arg323 = new BitSet(new long[]{0x0000000004008000L});
    public static final BitSet FOLLOW_typename_in_arg325 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_instr343 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_expr_in_instr346 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_instr348 = new BitSet(new long[]{0x00000E4800004000L});
    public static final BitSet FOLLOW_instr_in_instr351 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_instr354 = new BitSet(new long[]{0x00000E4800004000L});
    public static final BitSet FOLLOW_instr_in_instr357 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_instr369 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_expr_in_instr372 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_instr374 = new BitSet(new long[]{0x00000E4800004000L});
    public static final BitSet FOLLOW_instr_in_instr377 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_lvalue_in_instr387 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_instr389 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_expr_in_instr392 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_instr402 = new BitSet(new long[]{0x3880000100034002L});
    public static final BitSet FOLLOW_expr_in_instr405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDF_in_instr416 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_instr418 = new BitSet(new long[]{0x3880000300034000L});
    public static final BitSet FOLLOW_exprlist_in_instr420 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_instr423 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_instr446 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_lvalue_in_instr449 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_instr459 = new BitSet(new long[]{0x3000000000034000L});
    public static final BitSet FOLLOW_lvalue_in_instr463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_cste_in_instr465 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_instrsp474 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_expr_in_instrsp477 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_instrsp479 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_44_in_instrsp482 = new BitSet(new long[]{0x00003E4880404000L});
    public static final BitSet FOLLOW_block_in_instrsp485 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_instrsp488 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_instrsp492 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_44_in_instrsp495 = new BitSet(new long[]{0x00003E4880404000L});
    public static final BitSet FOLLOW_block_in_instrsp498 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_instrsp501 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_instrsp516 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_expr_in_instrsp519 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_instrsp521 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_44_in_instrsp524 = new BitSet(new long[]{0x00003E4880404000L});
    public static final BitSet FOLLOW_block_in_instrsp527 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_instrsp530 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_instrsp543 = new BitSet(new long[]{0x00003E4880404000L});
    public static final BitSet FOLLOW_block_in_instrsp546 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_instrsp549 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_sequence_in_block557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_instr_in_sequence574 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_sequence576 = new BitSet(new long[]{0x00001E4880404000L});
    public static final BitSet FOLLOW_instrsp_in_sequence579 = new BitSet(new long[]{0x00001E4880404000L});
    public static final BitSet FOLLOW_instr_in_sequence584 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_instrsp_in_sequence586 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDF_in_lvalue595 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_27_in_lvalue599 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_exprlist_in_lvalue602 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_lvalue604 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expr_in_exprlist615 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_25_in_exprlist618 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_expr_in_exprlist621 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_and_in_expr631 = new BitSet(new long[]{0x0000400000000002L});
    public static final BitSet FOLLOW_46_in_expr634 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_and_in_expr637 = new BitSet(new long[]{0x0000400000000002L});
    public static final BitSet FOLLOW_test_in_and646 = new BitSet(new long[]{0x0000800000000002L});
    public static final BitSet FOLLOW_47_in_and649 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_test_in_and652 = new BitSet(new long[]{0x0000800000000002L});
    public static final BitSet FOLLOW_add_in_test661 = new BitSet(new long[]{0x003F000000000002L});
    public static final BitSet FOLLOW_set_in_test664 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_add_in_test689 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_mult_in_add697 = new BitSet(new long[]{0x00C0000000000002L});
    public static final BitSet FOLLOW_set_in_add700 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_mult_in_add707 = new BitSet(new long[]{0x00C0000000000002L});
    public static final BitSet FOLLOW_pow_in_mult715 = new BitSet(new long[]{0x0300000000000002L});
    public static final BitSet FOLLOW_set_in_mult718 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_pow_in_mult725 = new BitSet(new long[]{0x0300000000000002L});
    public static final BitSet FOLLOW_neg_in_pow734 = new BitSet(new long[]{0x0400000000000002L});
    public static final BitSet FOLLOW_58_in_pow737 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_pow_in_pow740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_neg750 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_59_in_neg753 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_atom_in_neg758 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_cste_in_atom765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDF_in_atom771 = new BitSet(new long[]{0x0000000108000002L});
    public static final BitSet FOLLOW_32_in_atom776 = new BitSet(new long[]{0x3880000300034000L});
    public static final BitSet FOLLOW_exprlist_in_atom779 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_atom782 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_atom787 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_exprlist_in_atom790 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_atom792 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_atom802 = new BitSet(new long[]{0x3880000100034000L});
    public static final BitSet FOLLOW_expr_in_atom805 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_atom807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_cste0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_instr_in_synpred1_LEAC57 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_instr_in_synpred9_LEAC258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_synpred13_LEAC354 = new BitSet(new long[]{0x00000E4800004000L});
    public static final BitSet FOLLOW_instr_in_synpred13_LEAC357 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expr_in_synpred17_LEAC405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_instr_in_synpred30_LEAC574 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_synpred30_LEAC576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_instrsp_in_synpred31_LEAC579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_instr_in_synpred32_LEAC584 = new BitSet(new long[]{0x0000000000000002L});

}
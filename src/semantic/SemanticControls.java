package semantic;

import java.util.Stack;

import semantic.functions.FunctionArgControl;
import semantic.functions.FunctionReturnControl;
import semantic.scope.ExprScopeControl;
import semantic.scope.IdfScopeControl;
import semantic.type.ArrayType;
import semantic.type.ExprTypeControl;
import tds.Symbol;
import tds.Symbol.Type;
import ast.Node;

public class SemanticControls {

	private Node tree;
	private static int nb=0;
	public static boolean DEBUG=false;

	public SemanticControls(Node tree) {
		this.tree = tree;
	}

	public void execute() {
		Stack<Node> stack = new Stack<Node>();
		stack.add(tree);
		Node node;
		while (!stack.isEmpty()) {
			node = stack.pop();
			try {
				String label = node.getLabel();
				if (label.equals("=")) {// expr (recursive control)
					controlAffec(node);
				} else if (label.equals("FUNCTION")) { // function call without
														// getting the result
					new IdfScopeControl(node.getChild(0)).execute();
					new FunctionArgControl(node.getChild(0)).execute();
				}
				else if(label.equals("function")){ //declaration
					new FunctionReturnControl(node).execute();
				}
				else if (label.equals("read") || label.equals("write")) {
					new ExprScopeControl(node).execute();
					new ExprTypeControl(node).getTypeOfExpr();
				} else if (label.equals("while") || label.equals("if")) {
					new ExprScopeControl(node.getChild(0)).execute();
					Type type = new ExprTypeControl(node.getChild(0))
							.getTypeOfExpr();
					if (!type.equals(Type.BOOL)) {
						throw new SemanticControlsException(
								"Types non compatibles : boolean attendu pour instruction "
										+ label + ", " + type.toString()
										+ " donné", node.getChild(0).getLine());
					}
				}

			} catch (SemanticControlsException e) {
				System.err.println(e.getMessage());
				nb++;
			}
			for (int i = node.getChildCount() - 1; i >= 0; i--) {
				stack.add(node.getChild(i));
			}
		}
	}

	/**
	 * 
	 * @param node
	 *            The node labeled '='
	 * @throws SemanticControlsException
	 */
	private void controlAffec(Node node) throws SemanticControlsException {

		Node nodeL = node.getChild(0);
		Node nodeR = node.getChild(1);

		Symbol symbolL = null;

		/*** SCOPE CONTROL FOR LEFT IDF ***/
		IdfScopeControl idfScopeControl = new IdfScopeControl(nodeL);
		idfScopeControl.execute();
		symbolL = idfScopeControl.getSymbol();

		/*** BOUNDS CONTROL FOR LEFT IDF : ARRAY ***/
		if (symbolL.isArray()) {
			ArrayType.checkTypeOfBounds(nodeL, nodeL.getLine());
			ArrayType.controlArrayBounds(nodeL, symbolL);
		}

		/*** SCOPE CONTROL FOR RIGHT IDF ***/
		if (nodeR.getLabel().equals("IDF")) {
			idfScopeControl = new IdfScopeControl(nodeR);
			idfScopeControl.execute();
		}
		new ExprScopeControl(nodeR).execute();
		new ExprTypeControl(nodeL, symbolL, nodeR).execute();

	}
	public int getNumberOfSemanticErrors(){
		return nb;
	}
}

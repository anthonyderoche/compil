package semantic.scope;

import ast.Node;
import semantic.Control;
import semantic.SemanticControlsException;
import tds.Symbol;
import tds.TDS;

public class IdfScopeControl implements Control{

	private Node node;
	private Symbol symbol;
	public IdfScopeControl(Node node){
		this.node=node;
		this.symbol=null;
	}
	
	
	@Override
	public void execute() throws SemanticControlsException {
		// TODO Auto-generated method stub
			TDS tds = node.getTDS();
			String idf = node.getValue();
			boolean error = true;
			Symbol symbol = null;
			while (tds != null && error) { // browse all tds the symbol can access
				if (tds.containsKey(idf)) {
					error = false;
					symbol = tds.get(idf);
				}
				tds = tds.getFather();
			}
			if (error)
				throw new SemanticControlsException("Identifiant non déclaré : "
						+ idf, node.getLine());
			this.symbol=symbol;
	}
	
	public Symbol getSymbol(){
		if(symbol==null)
			throw new IllegalStateException("execute must be called before");
		return symbol;
	}

}

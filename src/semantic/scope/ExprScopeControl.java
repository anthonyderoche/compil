package semantic.scope;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import semantic.Control;
import semantic.SemanticControlsException;
import ast.Node;

public class ExprScopeControl implements Control {

	private Node nodeR;
	private IdfScopeControl idfScopeControl;

	public ExprScopeControl(Node nodeR) {
		this.nodeR = nodeR;
	}

	@Override
	public void execute() throws SemanticControlsException {
		List<SemanticControlsException> ecaught = new ArrayList<SemanticControlsException>();
		Stack<Node> stack = new Stack<Node>();
		stack.add(nodeR);
		while (!stack.isEmpty()) {
			Node node = stack.pop();
			if (node.getLabel().equals("IDF")) {
				try {
					idfScopeControl = new IdfScopeControl(node);
					idfScopeControl.execute();
					//Symbol symbol = idfScopeControl.getSymbol();

				} catch (SemanticControlsException e) {
					if (!ecaught.contains(e))
						ecaught.add(e);
				}
			}
			for (int i = node.getChildCount() - 1; i >= 0; i--) {
				stack.add(node.getChild(i));
			}
		}
		if (ecaught.size() > 0) {
			throw new SemanticControlsException(ecaught);
		}
	}

}

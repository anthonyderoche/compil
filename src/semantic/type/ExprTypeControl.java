package semantic.type;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import semantic.Control;
import semantic.SemanticControlsException;
import semantic.functions.FunctionArgControl;
import semantic.functions.FunctionReturnControl;
import tds.Symbol;
import tds.Symbol.Type;
import ast.Node;

public class ExprTypeControl implements Control {

	private Symbol symbolL;
	private int line;
	private Node nodeL;
	private Node nodeR;

	public ExprTypeControl(Node nodeL, Symbol symbolL, Node nodeR) {
		this.nodeL = nodeL;
		this.symbolL = symbolL;
		this.nodeR = nodeR;
		this.line = nodeR.getLine();
	}

	public ExprTypeControl(Node nodeR) {
		this.nodeR = nodeR;
		this.line = nodeR.getLine();
	}

	public void execute() throws SemanticControlsException {
		if (nodeL == null || symbolL == null) {
			throw new IllegalStateException();
		}
		Type type = getTypeOfExpr();
		if (symbolL.isArray()) {
			ArrayType.checkArrayType(symbolL, nodeL, type, line);
		} else {
			if (!type.equals(symbolL.getType())) {
				throw new SemanticControlsException("Types non compatibles : "
						+ symbolL.getType().toString() + " attendu pour "
						+ symbolL.getLabel() + ", " + type.toString()
						+ " donné", line);
			}
		}
	}

	public Type getTypeOfExpr() throws SemanticControlsException {

		Stack<Node> stack = new Stack<Node>();
		stack.add(nodeR);
		Node node = nodeR;
		Symbol symb = null;
		String currentOpe = null;
		Type type = Type.UNDEFINED;
		boolean isTest = false;
		List<Node> visited = new ArrayList<Node>();
		while (!stack.empty()) {

			node = stack.pop();

			if (isTest) {

				if (visited.contains(node.getFather())
						&& !currentOpe.equals(node.getFather().getLabel())) {
					if (intRes.contains(currentOpe)) {
						type = Type.INT;
					} else if (boolRes.contains(currentOpe)) {
						type = Type.BOOL;
					}
				} else {
					visited.add(node.getFather());
				}

				if (intOpe.contains(node.getValue())
						&& boolOpe.contains(node.getValue())) {
					type = new ExprTypeControl(node.getChild(0)).getTypeOfExpr();
				} else if (intOpe.contains(node.getValue())) {
					type = Type.INT;
				} else if (boolOpe.contains(node.getValue())) {
					type = Type.BOOL;
				}

				currentOpe = node.getFather().getLabel();
			}

			if (node.getLabel().equals("IDF")) {
				symb = Symbol.getSymbolFromNode(node);
				if (symb.isArray()) {
					int dimUsed = ArrayType.getDimensionUsed(node, symb, line);
					type = checkType(type, symb.getType().dim(dimUsed),
							currentOpe, line);
					ArrayType.checkTypeOfBounds(node, line);
					ArrayType.controlArrayBounds(node, symb);

				} else if (symb.getCategory().equals("FUNCTION")) {
					new FunctionArgControl(node).execute();
					type = checkType(type,symb.getType(), currentOpe, line);
				} else {
					type = checkType(type, symb.getType(), currentOpe, line);
				}
			} else if (node.getLabel().equals("INT")) {
				type = checkType(type, Type.INT, currentOpe, line);
			} else if (node.getLabel().equals("true")
					|| node.getLabel().equals("false")) {
				type = checkType(type, Type.BOOL, currentOpe, line);
			} else {

				isTest = true;
			}

			if (!(node.getLabel().equals("IDF") && (symb.getCategory().equals(
					"FUNCTION") || symb.isArray())))
				for (int i = node.getChildCount() - 1; i >= 0; i--) {
					stack.add(node.getChild(i));
				}
		}

		if (boolRes.contains(nodeR.getLabel()))
			type = Type.BOOL;
		if (intRes.contains(nodeR.getLabel()))
			type = Type.INT;

		return type;
	}

	private Type checkType(Type type, Type ctype, String currentOpe, int line)
			throws SemanticControlsException {
		if (type.equals(Type.UNDEFINED)) {
			type = ctype;
		} else {
			// System.out.println(type + currentOpe + ctype);
			String message;
			if (currentOpe != null)
				message = "Les types " + type.toString() + " et "
						+ ctype.toString()
						+ " sont incompatibles pour l'opérande " + currentOpe;
			else
				message = "Les types " + type.toString() + " et "
						+ ctype.toString() + " sont incompatibles";
			SemanticControlsException e = new SemanticControlsException(
					message, line);
			if (!ctype.equals(type))
				throw e;
			if (currentOpe != null) {
				if (intOpe.contains(currentOpe)
						&& !boolOpe.contains(currentOpe) && type != Type.INT)
					throw e;
				if (!intOpe.contains(currentOpe)
						&& boolOpe.contains(currentOpe) && type != Type.BOOL)
					throw e;
			}
		}
		return type;
	}

	private static final List<String> intOpe = Arrays.asList(new String[] {
			"+", "-", "*", "/", "<", ">", "<=", ">=", "==", "!=" });
	private static final List<String> boolOpe = Arrays.asList(new String[] {
			"and", "or", "==", "!=" });
	private static final List<String> boolRes = Arrays.asList(new String[] {
			"and", "or", "==", "!=", "<", ">", "<=", ">=","not" });
	private static final List<String> intRes = Arrays.asList(new String[] {
			"+", "-", "*", "/" });
}

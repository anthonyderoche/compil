package semantic.type;

import java.util.List;

import semantic.SemanticControlsException;
import tds.Symbol;
import tds.Symbol.Type;
import ast.Node;

public class ArrayType {

	public static int getDimensionUsed(Node node, Symbol symbol, int line)
			throws SemanticControlsException {
		List<Integer[]> bounds = symbol.getBounds();
		if (bounds.size() < node.getChildCount())
			throw new SemanticControlsException("Le tableau "
					+ symbol.getLabel() + " ne possède que " + bounds.size()
					+ " dimensions", line);
		return node.getChildCount();
	}

	public static void controlArrayBounds(Node node, Symbol symbol)
			throws SemanticControlsException {
		List<Integer[]> bounds = symbol.getBounds();
		int bou;
		for (int i = 0; i < node.getChildCount(); i++) {
			try {
				bou = Integer.parseInt(node.getChild(i).getValue());
				if (bou < bounds.get(i)[0] || bou > bounds.get(i)[1])
					throw new SemanticControlsException(
							"Accès en dehors des bornes du tableau "
									+ symbol.getLabel() + " pour la dimension "
									+ (i + 1) + ", " + bou
									+ " n'est pas compris entre "
									+ bounds.get(i)[0] + " et "
									+ bounds.get(i)[1], node.getLine());
			} catch (NumberFormatException e) {
				// bound is an idf
			}
		}
	}

	public static void checkArrayType(Symbol symbolL, Node nodeL,
			Type typeGiven, int line) throws SemanticControlsException {
		if (symbolL.isArray()) {
			int dimUsed = ArrayType.getDimensionUsed(nodeL, symbolL, line);
			String typeExpected = symbolL.getType().dim(dimUsed).toString();
			String label = symbolL.getLabel();
			if (!typeGiven.equals(symbolL.getType().dim(dimUsed))) {
					throw new SemanticControlsException("Types non compatibles : "
						+ typeExpected + " attendu pour " + label + ", "
						+ typeGiven.toString() + " donné", line);
			} else if (dimUsed != symbolL.getBounds().size()) {

				for (int i = 0; i < symbolL.getBounds().size() - dimUsed; i++) {
					typeExpected += "[]";
				}
				for (int i = 0; i < dimUsed; i++) {
					label += "[]";
				}
				throw new SemanticControlsException("Types non compatibles : "
						+ typeExpected + " attendu pour " + label + ", "
						+ typeGiven.toString() + " donné", line);

			}
		}
	}

	public static void checkTypeOfBounds(Node node, int line)
			throws SemanticControlsException {
		for (int i = 0; i < node.getChildCount(); i++) {
			Type type = new ExprTypeControl(node.getChild(i))
					.getTypeOfExpr();
			if (!type.equals(Type.INT))
				throw new SemanticControlsException(
						"Type incompatible avec une borne de tableau, " + type
								+ " donné au lieu de " + Type.INT, line);
		}
	}
}

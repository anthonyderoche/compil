package semantic;

import java.util.List;

@SuppressWarnings("serial")
public class SemanticControlsException extends Exception {

	private String message = "";

	public SemanticControlsException(String message, int line) {
		super("ligne "+line+" : "+message);
	}
	
	public SemanticControlsException(String message) {
		super(message);
	}

	public SemanticControlsException(List<SemanticControlsException> ecaught) {
		for (int i = 0; i < ecaught.size(); i++) {
			message += ecaught.get(i).getMessage();
			if (i < ecaught.size() - 1)
				message += "\n";
		}
	}
	
	@Override
	public String getMessage() {
		if (this.message.isEmpty()) {
			return super.getMessage();
		}
		return message;
	}
	
	public boolean equals(Object obj){
		if(obj!=null && obj instanceof SemanticControlsException){
			SemanticControlsException sem  = (SemanticControlsException)obj;
			return this.getMessage().equals(sem.getMessage());
		}
		return false;
	}

}

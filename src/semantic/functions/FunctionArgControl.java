package semantic.functions;

import semantic.Control;
import semantic.SemanticControlsException;
import semantic.type.ExprTypeControl;
import tds.Function;
import tds.Symbol;
import tds.Symbol.Type;
import ast.Node;

public class FunctionArgControl implements Control {

	private Node node;
	private Function function;

	public FunctionArgControl(Node node) {
		this.node = node;
		this.function = (Function) Symbol.getSymbolFromNode(node);
	}

	@Override
	public void execute() throws SemanticControlsException {
		// control number of arguments
		if (node.getChildCount() != function.getArgumentCount()) {
			String message = function.getArgumentCount()
					+ " arguments sont attendus pour la fonction "
					+ function.getLabel();
			if (function.getArgumentCount() == 0)
				message = "Aucun argument n'est attendu pour la fonction "
						+ function.getLabel();
			else if (function.getArgumentCount() == 1)
				message = function.getArgumentCount()
						+ " argument est attendu pour la fonction "
						+ function.getLabel();
			throw new SemanticControlsException(message, node.getLine());
		}

		// control type of argument
		for (int i = 0; i < node.getChildCount(); i++) {

			Type type = new ExprTypeControl(node.getChild(i))
					.getTypeOfExpr();
			if (!type.equals(function.getArg(i).getType())) {
				throw new SemanticControlsException("Le type de l'argument n°"
						+ (i + 1) + " de la fonction " + function.getLabel()
						+ " ne correspond pas à sa déclaration, " + type
						+ " donné au lieu de " + function.getArg(i).getType(),
						node.getLine());
			}
		}

	}

}

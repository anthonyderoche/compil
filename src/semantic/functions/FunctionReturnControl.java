package semantic.functions;

import semantic.Control;
import semantic.SemanticControlsException;
import semantic.type.ExprTypeControl;
import tds.Function;
import tds.Symbol;
import tds.Symbol.Type;
import ast.Node;

public class FunctionReturnControl implements Control {

	private Node node;
	private Function function;

	public FunctionReturnControl(Node node) {
		this.node = node;
		this.function = (Function) Symbol.getSymbolFromNode(node.getChild(0));
	}

	@Override
	public void execute() throws SemanticControlsException {

		Node sequence=null;
		for(int i=0;i<node.getChildCount();i++)
			if(node.getChild(i).getLabel().equals("SEQUENCE")){
				sequence = node.getChild(i);
				break;
			}
		
		
		if(sequence!=null){
			Node returnNode=null;
			int i;
			for(i=0;i<sequence.getChildCount();i++)
				if(sequence.getChild(i).getLabel().equals("return")){
					returnNode = sequence.getChild(i);
					break;
				}
				
			if(returnNode==null){
				if(!function.getType().equals(Type.VOID))
					throw new SemanticControlsException("Cette fonction doit retourner un résultat de type "+function.getType(),node.getLine());
			}
			else{
				Type returnType;
				if (returnNode.getChildCount() > 0) {
					returnType = new ExprTypeControl(returnNode.getChild(0))
							.getTypeOfExpr();
				} else {
					returnType = Type.VOID;
				}
		
				if (!function.getType().equals(returnType)) {
					throw new SemanticControlsException(
							"Type de retour non compatible : "+returnType+ " donné, "+function.getType()+" attendu", returnNode.getLine());
				}
				
				if(i+1<sequence.getChildCount()){
					Node nodeAfterReturn = sequence.getChild(i+1);
					throw new SemanticControlsException(
							"Code inatteignable", nodeAfterReturn.getLine());
				}
			}
		}
	}
}

package semantic;

public interface Control {
	public void execute() throws SemanticControlsException;
}

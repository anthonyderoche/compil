// $ANTLR 3.2 Sep 23, 2009 12:02:23 src/LEAC.g 2014-04-28 14:33:45

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class LEACLexer extends Lexer {
    public static final int FUNCTION=11;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int IDF=14;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int T__21=21;
    public static final int ARGLIST=8;
    public static final int T__61=61;
    public static final int T__60=60;
    public static final int EOF=-1;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int ARG=9;
    public static final int IDENTLIST=5;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int TXT=17;
    public static final int T__59=59;
    public static final int ATOMTYPE=15;
    public static final int T__50=50;
    public static final int ARRAY=13;
    public static final int FUNDECLIST=6;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RANGELIST=7;
    public static final int INT=16;
    public static final int T__30=30;
    public static final int SEQUENCE=12;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int WS=20;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int NEWLINE=19;
    public static final int T__36=36;
    public static final int COMMENTAIRE=18;
    public static final int T__37=37;
    public static final int ARGREF=10;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int VARDECLIST=4;

    // delegates
    // delegators

    public LEACLexer() {;} 
    public LEACLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public LEACLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "src/LEAC.g"; }

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:3:7: ( 'program' )
            // src/LEAC.g:3:9: 'program'
            {
            match("program"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:4:7: ( 'var' )
            // src/LEAC.g:4:9: 'var'
            {
            match("var"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:5:7: ( ':' )
            // src/LEAC.g:5:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:6:7: ( ';' )
            // src/LEAC.g:6:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:7:7: ( ',' )
            // src/LEAC.g:7:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:8:7: ( 'array' )
            // src/LEAC.g:8:9: 'array'
            {
            match("array"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:9:7: ( '[' )
            // src/LEAC.g:9:9: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:10:7: ( ']' )
            // src/LEAC.g:10:9: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:11:7: ( 'of' )
            // src/LEAC.g:11:9: 'of'
            {
            match("of"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:12:7: ( '..' )
            // src/LEAC.g:12:9: '..'
            {
            match(".."); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:13:7: ( 'function' )
            // src/LEAC.g:13:9: 'function'
            {
            match("function"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:14:7: ( '(' )
            // src/LEAC.g:14:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:15:7: ( ')' )
            // src/LEAC.g:15:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:16:7: ( 'ref' )
            // src/LEAC.g:16:9: 'ref'
            {
            match("ref"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:17:7: ( 'if' )
            // src/LEAC.g:17:9: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:18:7: ( 'then' )
            // src/LEAC.g:18:9: 'then'
            {
            match("then"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:19:7: ( 'else' )
            // src/LEAC.g:19:9: 'else'
            {
            match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:20:7: ( 'while' )
            // src/LEAC.g:20:9: 'while'
            {
            match("while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:21:7: ( 'do' )
            // src/LEAC.g:21:9: 'do'
            {
            match("do"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:22:7: ( '=' )
            // src/LEAC.g:22:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:23:7: ( 'return' )
            // src/LEAC.g:23:9: 'return'
            {
            match("return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:24:7: ( 'read' )
            // src/LEAC.g:24:9: 'read'
            {
            match("read"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:25:7: ( 'write' )
            // src/LEAC.g:25:9: 'write'
            {
            match("write"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:26:7: ( '{' )
            // src/LEAC.g:26:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:27:7: ( '}' )
            // src/LEAC.g:27:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:28:7: ( 'or' )
            // src/LEAC.g:28:9: 'or'
            {
            match("or"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:29:7: ( 'and' )
            // src/LEAC.g:29:9: 'and'
            {
            match("and"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:30:7: ( '<' )
            // src/LEAC.g:30:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:31:7: ( '<=' )
            // src/LEAC.g:31:9: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:32:7: ( '>' )
            // src/LEAC.g:32:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:33:7: ( '>=' )
            // src/LEAC.g:33:9: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:34:7: ( '==' )
            // src/LEAC.g:34:9: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:35:7: ( '!=' )
            // src/LEAC.g:35:9: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:36:7: ( '+' )
            // src/LEAC.g:36:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:37:7: ( '-' )
            // src/LEAC.g:37:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "T__56"
    public final void mT__56() throws RecognitionException {
        try {
            int _type = T__56;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:38:7: ( '*' )
            // src/LEAC.g:38:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__56"

    // $ANTLR start "T__57"
    public final void mT__57() throws RecognitionException {
        try {
            int _type = T__57;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:39:7: ( '/' )
            // src/LEAC.g:39:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__57"

    // $ANTLR start "T__58"
    public final void mT__58() throws RecognitionException {
        try {
            int _type = T__58;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:40:7: ( '^' )
            // src/LEAC.g:40:9: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__58"

    // $ANTLR start "T__59"
    public final void mT__59() throws RecognitionException {
        try {
            int _type = T__59;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:41:7: ( 'not' )
            // src/LEAC.g:41:9: 'not'
            {
            match("not"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__59"

    // $ANTLR start "T__60"
    public final void mT__60() throws RecognitionException {
        try {
            int _type = T__60;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:42:7: ( 'true' )
            // src/LEAC.g:42:9: 'true'
            {
            match("true"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__60"

    // $ANTLR start "T__61"
    public final void mT__61() throws RecognitionException {
        try {
            int _type = T__61;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:43:7: ( 'false' )
            // src/LEAC.g:43:9: 'false'
            {
            match("false"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__61"

    // $ANTLR start "ATOMTYPE"
    public final void mATOMTYPE() throws RecognitionException {
        try {
            int _type = ATOMTYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:16:10: ( 'void' | 'bool' | 'int' )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 'v':
                {
                alt1=1;
                }
                break;
            case 'b':
                {
                alt1=2;
                }
                break;
            case 'i':
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // src/LEAC.g:16:12: 'void'
                    {
                    match("void"); 


                    }
                    break;
                case 2 :
                    // src/LEAC.g:16:21: 'bool'
                    {
                    match("bool"); 


                    }
                    break;
                case 3 :
                    // src/LEAC.g:16:29: 'int'
                    {
                    match("int"); 


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ATOMTYPE"

    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:60:5: ( ( '-' )? ( '0' .. '9' )+ )
            // src/LEAC.g:60:7: ( '-' )? ( '0' .. '9' )+
            {
            // src/LEAC.g:60:7: ( '-' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='-') ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // src/LEAC.g:60:7: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // src/LEAC.g:60:11: ( '0' .. '9' )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // src/LEAC.g:60:11: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "INT"

    // $ANTLR start "TXT"
    public final void mTXT() throws RecognitionException {
        try {
            int _type = TXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:62:5: ( '\"' ( 'a' .. 'z' | 'A' .. 'Z' | ' ' | '0' .. '9' | '.' | '?' | '!' | '(' | ')' | '\\'' )+ '\"' )
            // src/LEAC.g:62:7: '\"' ( 'a' .. 'z' | 'A' .. 'Z' | ' ' | '0' .. '9' | '.' | '?' | '!' | '(' | ')' | '\\'' )+ '\"'
            {
            match('\"'); 
            // src/LEAC.g:62:10: ( 'a' .. 'z' | 'A' .. 'Z' | ' ' | '0' .. '9' | '.' | '?' | '!' | '(' | ')' | '\\'' )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=' ' && LA4_0<='!')||(LA4_0>='\'' && LA4_0<=')')||LA4_0=='.'||(LA4_0>='0' && LA4_0<='9')||LA4_0=='?'||(LA4_0>='A' && LA4_0<='Z')||(LA4_0>='a' && LA4_0<='z')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // src/LEAC.g:
            	    {
            	    if ( (input.LA(1)>=' ' && input.LA(1)<='!')||(input.LA(1)>='\'' && input.LA(1)<=')')||input.LA(1)=='.'||(input.LA(1)>='0' && input.LA(1)<='9')||input.LA(1)=='?'||(input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);

            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "TXT"

    // $ANTLR start "IDF"
    public final void mIDF() throws RecognitionException {
        try {
            int _type = IDF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:64:5: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )* )
            // src/LEAC.g:64:9: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // src/LEAC.g:64:29: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')||(LA5_0>='A' && LA5_0<='Z')||(LA5_0>='a' && LA5_0<='z')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // src/LEAC.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "IDF"

    // $ANTLR start "COMMENTAIRE"
    public final void mCOMMENTAIRE() throws RecognitionException {
        try {
            int _type = COMMENTAIRE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:66:12: ( '/*' TXT '*/' )
            // src/LEAC.g:66:14: '/*' TXT '*/'
            {
            match("/*"); 

            mTXT(); 
            match("*/"); 

            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMENTAIRE"

    // $ANTLR start "NEWLINE"
    public final void mNEWLINE() throws RecognitionException {
        try {
            int _type = NEWLINE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:67:8: ( ( '\\r' )? '\\n' )
            // src/LEAC.g:67:9: ( '\\r' )? '\\n'
            {
            // src/LEAC.g:67:9: ( '\\r' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0=='\r') ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // src/LEAC.g:67:9: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 
            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "NEWLINE"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // src/LEAC.g:68:5: ( ( ' ' | '\\t' )+ )
            // src/LEAC.g:68:9: ( ' ' | '\\t' )+
            {
            // src/LEAC.g:68:9: ( ' ' | '\\t' )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0=='\t'||LA7_0==' ') ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // src/LEAC.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);

            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // src/LEAC.g:1:8: ( T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | ATOMTYPE | INT | TXT | IDF | COMMENTAIRE | NEWLINE | WS )
        int alt8=48;
        alt8 = dfa8.predict(input);
        switch (alt8) {
            case 1 :
                // src/LEAC.g:1:10: T__21
                {
                mT__21(); 

                }
                break;
            case 2 :
                // src/LEAC.g:1:16: T__22
                {
                mT__22(); 

                }
                break;
            case 3 :
                // src/LEAC.g:1:22: T__23
                {
                mT__23(); 

                }
                break;
            case 4 :
                // src/LEAC.g:1:28: T__24
                {
                mT__24(); 

                }
                break;
            case 5 :
                // src/LEAC.g:1:34: T__25
                {
                mT__25(); 

                }
                break;
            case 6 :
                // src/LEAC.g:1:40: T__26
                {
                mT__26(); 

                }
                break;
            case 7 :
                // src/LEAC.g:1:46: T__27
                {
                mT__27(); 

                }
                break;
            case 8 :
                // src/LEAC.g:1:52: T__28
                {
                mT__28(); 

                }
                break;
            case 9 :
                // src/LEAC.g:1:58: T__29
                {
                mT__29(); 

                }
                break;
            case 10 :
                // src/LEAC.g:1:64: T__30
                {
                mT__30(); 

                }
                break;
            case 11 :
                // src/LEAC.g:1:70: T__31
                {
                mT__31(); 

                }
                break;
            case 12 :
                // src/LEAC.g:1:76: T__32
                {
                mT__32(); 

                }
                break;
            case 13 :
                // src/LEAC.g:1:82: T__33
                {
                mT__33(); 

                }
                break;
            case 14 :
                // src/LEAC.g:1:88: T__34
                {
                mT__34(); 

                }
                break;
            case 15 :
                // src/LEAC.g:1:94: T__35
                {
                mT__35(); 

                }
                break;
            case 16 :
                // src/LEAC.g:1:100: T__36
                {
                mT__36(); 

                }
                break;
            case 17 :
                // src/LEAC.g:1:106: T__37
                {
                mT__37(); 

                }
                break;
            case 18 :
                // src/LEAC.g:1:112: T__38
                {
                mT__38(); 

                }
                break;
            case 19 :
                // src/LEAC.g:1:118: T__39
                {
                mT__39(); 

                }
                break;
            case 20 :
                // src/LEAC.g:1:124: T__40
                {
                mT__40(); 

                }
                break;
            case 21 :
                // src/LEAC.g:1:130: T__41
                {
                mT__41(); 

                }
                break;
            case 22 :
                // src/LEAC.g:1:136: T__42
                {
                mT__42(); 

                }
                break;
            case 23 :
                // src/LEAC.g:1:142: T__43
                {
                mT__43(); 

                }
                break;
            case 24 :
                // src/LEAC.g:1:148: T__44
                {
                mT__44(); 

                }
                break;
            case 25 :
                // src/LEAC.g:1:154: T__45
                {
                mT__45(); 

                }
                break;
            case 26 :
                // src/LEAC.g:1:160: T__46
                {
                mT__46(); 

                }
                break;
            case 27 :
                // src/LEAC.g:1:166: T__47
                {
                mT__47(); 

                }
                break;
            case 28 :
                // src/LEAC.g:1:172: T__48
                {
                mT__48(); 

                }
                break;
            case 29 :
                // src/LEAC.g:1:178: T__49
                {
                mT__49(); 

                }
                break;
            case 30 :
                // src/LEAC.g:1:184: T__50
                {
                mT__50(); 

                }
                break;
            case 31 :
                // src/LEAC.g:1:190: T__51
                {
                mT__51(); 

                }
                break;
            case 32 :
                // src/LEAC.g:1:196: T__52
                {
                mT__52(); 

                }
                break;
            case 33 :
                // src/LEAC.g:1:202: T__53
                {
                mT__53(); 

                }
                break;
            case 34 :
                // src/LEAC.g:1:208: T__54
                {
                mT__54(); 

                }
                break;
            case 35 :
                // src/LEAC.g:1:214: T__55
                {
                mT__55(); 

                }
                break;
            case 36 :
                // src/LEAC.g:1:220: T__56
                {
                mT__56(); 

                }
                break;
            case 37 :
                // src/LEAC.g:1:226: T__57
                {
                mT__57(); 

                }
                break;
            case 38 :
                // src/LEAC.g:1:232: T__58
                {
                mT__58(); 

                }
                break;
            case 39 :
                // src/LEAC.g:1:238: T__59
                {
                mT__59(); 

                }
                break;
            case 40 :
                // src/LEAC.g:1:244: T__60
                {
                mT__60(); 

                }
                break;
            case 41 :
                // src/LEAC.g:1:250: T__61
                {
                mT__61(); 

                }
                break;
            case 42 :
                // src/LEAC.g:1:256: ATOMTYPE
                {
                mATOMTYPE(); 

                }
                break;
            case 43 :
                // src/LEAC.g:1:265: INT
                {
                mINT(); 

                }
                break;
            case 44 :
                // src/LEAC.g:1:269: TXT
                {
                mTXT(); 

                }
                break;
            case 45 :
                // src/LEAC.g:1:273: IDF
                {
                mIDF(); 

                }
                break;
            case 46 :
                // src/LEAC.g:1:277: COMMENTAIRE
                {
                mCOMMENTAIRE(); 

                }
                break;
            case 47 :
                // src/LEAC.g:1:289: NEWLINE
                {
                mNEWLINE(); 

                }
                break;
            case 48 :
                // src/LEAC.g:1:297: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA8 dfa8 = new DFA8(this);
    static final String DFA8_eotS =
        "\1\uffff\2\43\3\uffff\1\43\2\uffff\1\43\1\uffff\1\43\2\uffff\6"+
        "\43\1\71\2\uffff\1\73\1\75\2\uffff\1\76\1\uffff\1\100\1\uffff\2"+
        "\43\5\uffff\5\43\1\110\1\111\3\43\1\117\6\43\1\126\11\uffff\3\43"+
        "\1\132\2\43\1\135\2\uffff\2\43\1\140\2\43\1\uffff\1\143\5\43\1\uffff"+
        "\1\151\2\43\1\uffff\1\143\1\43\1\uffff\2\43\1\uffff\1\43\1\160\1"+
        "\uffff\1\161\1\162\1\163\2\43\1\uffff\1\143\1\43\1\167\1\43\1\171"+
        "\1\43\4\uffff\1\173\1\174\1\43\1\uffff\1\43\1\uffff\1\177\2\uffff"+
        "\1\u0080\1\43\2\uffff\1\u0082\1\uffff";
    static final String DFA8_eofS =
        "\u0083\uffff";
    static final String DFA8_minS =
        "\1\11\1\162\1\141\3\uffff\1\156\2\uffff\1\146\1\uffff\1\141\2\uffff"+
        "\1\145\1\146\1\150\1\154\1\150\1\157\1\75\2\uffff\2\75\2\uffff\1"+
        "\60\1\uffff\1\52\1\uffff\2\157\5\uffff\1\157\1\162\1\151\1\162\1"+
        "\144\2\60\1\156\1\154\1\141\1\60\1\164\1\145\1\165\1\163\2\151\1"+
        "\60\11\uffff\1\164\1\157\1\147\1\60\1\144\1\141\1\60\2\uffff\1\143"+
        "\1\163\1\60\1\165\1\144\1\uffff\1\60\1\156\2\145\1\154\1\164\1\uffff"+
        "\1\60\1\154\1\162\1\uffff\1\60\1\171\1\uffff\1\164\1\145\1\uffff"+
        "\1\162\1\60\1\uffff\3\60\2\145\1\uffff\1\60\1\141\1\60\1\151\1\60"+
        "\1\156\4\uffff\2\60\1\155\1\uffff\1\157\1\uffff\1\60\2\uffff\1\60"+
        "\1\156\2\uffff\1\60\1\uffff";
    static final String DFA8_maxS =
        "\1\175\1\162\1\157\3\uffff\1\162\2\uffff\1\162\1\uffff\1\165\2"+
        "\uffff\1\145\1\156\1\162\1\154\1\162\1\157\1\75\2\uffff\2\75\2\uffff"+
        "\1\71\1\uffff\1\52\1\uffff\2\157\5\uffff\1\157\1\162\1\151\1\162"+
        "\1\144\2\172\1\156\1\154\1\164\1\172\1\164\1\145\1\165\1\163\2\151"+
        "\1\172\11\uffff\1\164\1\157\1\147\1\172\1\144\1\141\1\172\2\uffff"+
        "\1\143\1\163\1\172\1\165\1\144\1\uffff\1\172\1\156\2\145\1\154\1"+
        "\164\1\uffff\1\172\1\154\1\162\1\uffff\1\172\1\171\1\uffff\1\164"+
        "\1\145\1\uffff\1\162\1\172\1\uffff\3\172\2\145\1\uffff\1\172\1\141"+
        "\1\172\1\151\1\172\1\156\4\uffff\2\172\1\155\1\uffff\1\157\1\uffff"+
        "\1\172\2\uffff\1\172\1\156\2\uffff\1\172\1\uffff";
    static final String DFA8_acceptS =
        "\3\uffff\1\3\1\4\1\5\1\uffff\1\7\1\10\1\uffff\1\12\1\uffff\1\14"+
        "\1\15\7\uffff\1\30\1\31\2\uffff\1\41\1\42\1\uffff\1\44\1\uffff\1"+
        "\46\2\uffff\1\53\1\54\1\55\1\57\1\60\22\uffff\1\40\1\24\1\35\1\34"+
        "\1\37\1\36\1\43\1\56\1\45\7\uffff\1\11\1\32\5\uffff\1\17\6\uffff"+
        "\1\23\3\uffff\1\2\2\uffff\1\33\2\uffff\1\16\2\uffff\1\52\5\uffff"+
        "\1\47\6\uffff\1\26\1\20\1\50\1\21\3\uffff\1\6\1\uffff\1\51\1\uffff"+
        "\1\22\1\27\2\uffff\1\25\1\1\1\uffff\1\13";
    static final String DFA8_specialS =
        "\u0083\uffff}>";
    static final String[] DFA8_transitionS = {
            "\1\45\1\44\2\uffff\1\44\22\uffff\1\45\1\31\1\42\5\uffff\1\14"+
            "\1\15\1\34\1\32\1\5\1\33\1\12\1\35\12\41\1\3\1\4\1\27\1\24\1"+
            "\30\2\uffff\32\43\1\7\1\uffff\1\10\1\36\2\uffff\1\6\1\40\1\43"+
            "\1\23\1\21\1\13\2\43\1\17\4\43\1\37\1\11\1\1\1\43\1\16\1\43"+
            "\1\20\1\43\1\2\1\22\3\43\1\25\1\uffff\1\26",
            "\1\46",
            "\1\47\15\uffff\1\50",
            "",
            "",
            "",
            "\1\52\3\uffff\1\51",
            "",
            "",
            "\1\53\13\uffff\1\54",
            "",
            "\1\56\23\uffff\1\55",
            "",
            "",
            "\1\57",
            "\1\60\7\uffff\1\61",
            "\1\62\11\uffff\1\63",
            "\1\64",
            "\1\65\11\uffff\1\66",
            "\1\67",
            "\1\70",
            "",
            "",
            "\1\72",
            "\1\74",
            "",
            "",
            "\12\41",
            "",
            "\1\77",
            "",
            "\1\101",
            "\1\102",
            "",
            "",
            "",
            "",
            "",
            "\1\103",
            "\1\104",
            "\1\105",
            "\1\106",
            "\1\107",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\112",
            "\1\113",
            "\1\116\4\uffff\1\114\15\uffff\1\115",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\120",
            "\1\121",
            "\1\122",
            "\1\123",
            "\1\124",
            "\1\125",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\127",
            "\1\130",
            "\1\131",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\133",
            "\1\134",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "",
            "",
            "\1\136",
            "\1\137",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\141",
            "\1\142",
            "",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\147",
            "\1\150",
            "",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\152",
            "\1\153",
            "",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\154",
            "",
            "\1\155",
            "\1\156",
            "",
            "\1\157",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\164",
            "\1\165",
            "",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\166",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\170",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\172",
            "",
            "",
            "",
            "",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\175",
            "",
            "\1\176",
            "",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "",
            "",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            "\1\u0081",
            "",
            "",
            "\12\43\7\uffff\32\43\6\uffff\32\43",
            ""
    };

    static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
    static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
    static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
    static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
    static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
    static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
    static final short[][] DFA8_transition;

    static {
        int numStates = DFA8_transitionS.length;
        DFA8_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
        }
    }

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = DFA8_eot;
            this.eof = DFA8_eof;
            this.min = DFA8_min;
            this.max = DFA8_max;
            this.accept = DFA8_accept;
            this.special = DFA8_special;
            this.transition = DFA8_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | ATOMTYPE | INT | TXT | IDF | COMMENTAIRE | NEWLINE | WS );";
        }
    }
 

}
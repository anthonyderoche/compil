grammar LEAC;

options {backtrack = true;output=AST; }
tokens {VARDECLIST;IDENTLIST;FUNDECLIST;RANGELIST;ARGLIST;ARG;ARGREF;FUNCTION;SEQUENCE;ARRAY;}

program: 'program'^ IDF vardeclist fundeclist (instr|instrsp);

vardeclist : (varsuitdecl+ -> ^(VARDECLIST varsuitdecl+))?;

varsuitdecl : 'var'^ identlist ':'! typename ';'!;

identlist : IDF (',' IDF)* -> ^(IDENTLIST IDF+);

typename : ATOMTYPE|arraytype;

ATOMTYPE : 'void' | 'bool' |'int';

arraytype : 'array' '[' rangelist (',' rangelist)* ']' 'of' ATOMTYPE ->^(ARRAY rangelist+ ATOMTYPE);

rangelist : INT '..' INT -> ^(RANGELIST INT INT);

fundeclist : ((fundecl)+ -> ^(FUNDECLIST fundecl+))?;

fundecl : 'function'^ IDF '('! arglist ')'! ':'! ATOMTYPE vardeclist (instr|instrsp) ;

arglist : (arg (',' arg)* -> ^(ARGLIST arg+))?;

arg : IDF ':' typename -> ^(ARG IDF typename)
    | 'ref' IDF ':' typename -> ^(ARGREF IDF typename);

instr : 'if'^ expr 'then'! instr ('else'! instr)?
      | 'while'^ expr 'do'! instr
      | lvalue '='^ expr
      | 'return'^ expr?
      | IDF '(' exprlist? ')' -> ^(FUNCTION ^(IDF exprlist?))
      | 'read'^ lvalue
      | 'write'^ (lvalue|cste);

instrsp : 'if'^ expr 'then'! '{'! block? '}'! ('else'! '{'! block? '}'!)?
        | 'while'^ expr 'do'! '{'! block? '}'!
        | '{'! block? '}'!;

block: sequence -> ^(SEQUENCE sequence);

sequence : (instr ';'!|instrsp)* (instr|instrsp);

lvalue : IDF^ ('['! exprlist ']'!)?;

exprlist : expr (','! expr)*;

expr : and ('or'^ and)*;
and : test ('and'^ test)*;
test : add (('<' | '<=' | '>' | '>=' | '==' | '!=')^ add)?;
add: mult (('+'|'-')^ mult)*;
mult: pow (('*'|'/')^ pow)*;
pow : neg ('^'^ pow)?;
neg : ('-'^|'not'^)? atom;
atom : cste | ( IDF^ ( '('! exprlist? ')'! | '['! exprlist ']'! )? ) | '('! expr ')'!;

INT : '-'?'0'..'9'+;

TXT : '"'('a'..'z'|'A'..'Z'|' '|'0'..'9'|'.'|'?'|'!'|'('|')'|'\'')+'"';
cste : INT | 'true' | 'false' | TXT;
IDF :   ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ;

COMMENTAIRE: '/*' TXT '*/'{$channel=HIDDEN;};
NEWLINE:'\r'? '\n'{$channel=HIDDEN;} ;
WS  :   (' '|'\t')+ {$channel=HIDDEN;};
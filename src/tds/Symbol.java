package tds;

import java.util.ArrayList;
import java.util.List;

import ast.Node;

public abstract class Symbol {

	private Object value;
	private String label;
	private Type type;
	private int offset;
	private List<Integer[]> bounds;

	public Symbol(String label, Type type) {
		this.label = label;
		this.type = type;
		bounds = new ArrayList<Integer[]>();
	}

	public Symbol(String label, Type type, List<Integer[]> bounds) {
		this(label, type);
		this.bounds = bounds;
	}

	public abstract String getCategory();

	public String toString() {
		String s = this.getCategory();
		s += " | " + type;
		s += " | " + label;

		if (bounds.size() > 0) {
			s += " | ";
			for (Integer[] bou : bounds) {
				s += "[" + bou[0] + ".." + bou[1] + "]";
			}
		}

		return s;
	}

	public Object getValue() {
		return value;
	}

	public String getLabel() {
		return label;
	}

	
	public Type getType() {
		return type;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getOffset() {
		return offset;
	}

	public boolean isArray() {
		return type.isArray();
	}

	public List<Integer[]> getBounds() {
		return bounds;
	}

	public void setBounds(List<Integer[]> bounds) {
		this.bounds = bounds;
	}

	public static Symbol getSymbolFromNode(Node node) {
		TDS tds = node.getTDS();
		if (tds == null)
			throw new IllegalStateException("TDS manquante pour le noeud"
					+ node.getLabel());

		while (tds != null) {
			if (tds.containsKey(node.getValue())) {
				return tds.get(node.getValue());
			}
			tds = tds.getFather();
		}

		return null;
	}

	public static class Type {
		private final static int UNDEFINED_ID = 0;
		private final static int INT_ID = 1;
		private final static int BOOL_ID = 2;
		private final static int VOID_ID = 3;
		private final static int ARRAY_ID = 4;
		

		public final static Type UNDEFINED = new Type(UNDEFINED_ID);
		public final static Type INT = new Type(INT_ID);
		public final static Type BOOL = new Type(BOOL_ID);
		public final static Type VOID = new Type(VOID_ID);
		public final static Type ARRAY = new Type(ARRAY_ID);
		public final static Type ARRAY_OF_INT = new Type(ARRAY_ID, INT_ID,1);
		public final static Type ARRAY_OF_BOOL = new Type(ARRAY_ID, BOOL_ID,1);
		
		public static Type ARRAY_OF_INT(int dim){
			return new Type(ARRAY_ID, INT_ID,dim);
		}
		public static Type ARRAY_OF_BOOL(int dim){
			return new Type(ARRAY_ID, BOOL_ID,dim);
		}
		
		private final static String NAMES[] = { "undefined", "int", "boolean",
				"void", "array" };

		private int type;
		private int dimension;
		private int arrayType;

		public Type(int type, int arrayType, int dim) {
			this.type = type;
			this.arrayType = arrayType;
			this.dimension=dim;
		}

		private Type(int type) {
			this.type = type;
			this.dimension=0;
		}

		public boolean equals(Object obj) {
			if (obj != null && obj instanceof Type) {
				Type t = (Type) obj;
				if(this.type == ARRAY_ID  && t.type==ARRAY_ID && t.dimension!=this.dimension){
					return false;
				}
				else if (this.type == ARRAY_ID && t.type != ARRAY_ID) {
					if (this.dimension != 0 && this.arrayType==t.type)
						return false;
					else if(t.type!=this.arrayType && t.type!=Type.UNDEFINED_ID)
						return false;
				}
				else if(this.type != ARRAY_ID && t.type == ARRAY_ID){
					if (t.dimension != 0 && this.type==t.arrayType)
						return false;
					else if(this.type!=t.arrayType && this.type!=Type.UNDEFINED_ID)
						return false;
				}
				else if (t.type != this.type){
					return false;
				}

				return true;
			}
			return false;
		}

		public int getDimension() {
			if (type != ARRAY_ID)
				throw new IllegalStateException(
						"To get the dimension, it must be an array");
			return dimension;
		}

		public void setDimension(int dimension) {
			if (type != ARRAY_ID)
				throw new IllegalStateException(
						"To set the dimension, it must be an array");
			this.dimension = dimension;
		}

		public String toString() {
			String s;
			if (type == ARRAY_ID) {
				s = NAMES[arrayType];
				for(int i=0;i<dimension;i++){
					s+="[]";
				}
			}else{
				s = NAMES[type];
			}
			return s;
		}
		
		public boolean isArray(){
			return type == ARRAY_ID;
		}
		
		public Type dim(int dim){
			return new Type(type,arrayType,dimension-dim);
		}
	};
}

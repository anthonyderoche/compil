package tds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class TDS extends HashMap<String, Symbol> {

	private List<TDS> children;

	private int depth;
	private int region;

	private TDS father;
	private static int count=0;

	public TDS() {
		children = new ArrayList<TDS>();
		father = null;
		region = count++;
	}

	public int getDepth() {
		return depth;
	}
	
	

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public List<TDS> getChildren() {
		return children;
	}

	public TDS getChild(int i) {
		return children.get(i);
	}

	public void addChild(TDS child) {
		child.father = this;
		children.add(child);
	}

	public int getChildCount() {
		return children.size();
	}

	public boolean isRoot() {
		return father == null;
	}

	public TDS getFather() {
		return father;
	}

	public String toString() {
		String s = "Table des symboles (région " + region + ")\n";
		s += "---------------------------\n";
		int i = 0;
		for (Map.Entry<String, Symbol> entry : this.entrySet()) {
			s += i++ +" | "+entry.getValue().toString() + "\n";
		}
		s += "---------------------------\n";
		
		/*for (TDS child : children) {
			s += child.toString();
		}*/
		
		TDS f=this.father;
		while(f!=null){
			s+= father.toString();
			f=f.father;
		}
		return s;
	}

	public int getRegion() {
		return region;
	}

	public void addBrother(TDS brother) {
		father.addChild(brother);
	}
	
	@Override
	public Symbol put(String key,Symbol symbol){
		Symbol previous = super.put(key, symbol);
		return previous;
	}

}

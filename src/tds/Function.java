package tds;

import java.util.ArrayList;
import java.util.List;

public class Function extends Symbol {

	public List<ARG> args;
	public Function(String label, Type type) {
		super(label, type);
		args = new ArrayList<ARG>();
	}

	private static final String CATE = "FUNCTION";

	@Override
	public String getCategory() {
		// TODO Auto-generated method stub
		return CATE;
	}
	
	public int getArgumentCount(){
		return args.size();
	}

	public void addArg(ARG arg) {
		args.add(arg);
	}

	public ARG getArg(int i) {
		// TODO Auto-generated method stub
		return args.get(i);
	}

}

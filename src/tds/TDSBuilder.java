package tds;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.tree.Tree;

import tds.Symbol.Type;

public class TDSBuilder {

	private TDS TDS;
	private TDS currentTDS;

	public TDSBuilder() {
		TDS = new TDS();
		TDS.setDepth(0);
		currentTDS = TDS;
	}

	/**
	 * Add a list of variables to the symbol table
	 * 
	 * @param node
	 *            The node labeled 'var'
	 */
	public void handleVar(Tree node) {
		String type = node.getChild(1).getText();
		Tree identList = node.getChild(0);

		for (int i = 0; i < identList.getChildCount(); i++) {
			String var = identList.getChild(i).getText();
			if (type != "ARRAY") {
				switch (type) {
				case "int":
					currentTDS.put(var, new IDF(var, Type.INT));
					break;
				case "bool":
					currentTDS.put(var, new IDF(var, Type.BOOL));
					break;
				}
			} else
				this.handleArray(node.getChild(1), var, false, null);
		}
	}

	/**
	 * 
	 * @return The global symbol table
	 */
	public TDS getTDS() {
		return TDS;
	}

	/**
	 * Create a new region considered encompassed in the current one
	 * 
	 * @param depth
	 */
	private void newRegion(int depth) {
		TDS newTDS = new TDS();
		newTDS.setDepth(depth);
		currentTDS.addChild(newTDS);
		currentTDS = newTDS;
	}

	/**
	 * Add a function to the symbol table
	 * 
	 * @param first
	 *            The node labeled 'function'
	 * @param depth
	 *            The depth of this node in the global tree
	 */
	public void handleFunction(Tree first, int depth) {
		// TODO Auto-generated method stub
		String label = first.getChild(0).getText();
		String type = "";
		boolean arg = false;
		if (first.getChild(1).getText().equals("ARGLIST")) {
			arg = true;
			type = first.getChild(2).getText();
		} else {
			type = first.getChild(1).getText();
		}
		Function f;
		switch (type) {
		case "int":
			f = new Function(label, Type.INT);
			break;
		case "bool":
			f = new Function(label, Type.BOOL);
			break;
		case "void":
			f = new Function(label, Type.VOID);
			break;
		default:
			f = new Function(label, Type.VOID);
			break;
		}
		currentTDS.put(label, f);
		this.newRegion(depth);

		if (arg)
			this.handleArg(first.getChild(1), (Function) f);
	}

	/**
	 * Add a list of arguments in the symbol table
	 * 
	 * @param node
	 *            The node labeled 'ARGLIST'
	 */
	private void handleArg(Tree node, Function f) {
		for (int i = 0; i < node.getChildCount(); i++) {
			String type = node.getChild(i).getChild(1).getText();
			String var = node.getChild(i).getChild(0).getText();
			if (type != "ARRAY") {
				ARG arg;
				switch (type) {
				case "int":
					arg = new ARG(var, Type.INT);
					f.addArg(arg);
					currentTDS.put(var, arg);
					break;
				case "bool":
					arg = new ARG(var, Type.BOOL);
					f.addArg(arg);
					currentTDS.put(var, arg);
					break;
				}
			} else
				this.handleArray(node.getChild(i).getChild(1), var, true, f);
		}
	}

	/**
	 * Add an array in the symbol table
	 * 
	 * @param node
	 *            The node labeled 'ARRAY'
	 * @param var
	 *            The label of the variable referencing the array
	 * @param arg
	 *            True if it's a function argument, otherwise false
	 */
	private void handleArray(Tree node, String var, boolean arg, Function f) {
		int i = 0;
		List<Integer[]> bounds = new ArrayList<Integer[]>();
		while (node.getChild(i).getText().equals("RANGELIST")) {
			bounds.add(new Integer[] {
					Integer.parseInt(node.getChild(i).getChild(0).getText()),
					Integer.parseInt(node.getChild(i).getChild(1).getText()) });
			i++;
		}
		String type = node.getChild(i).getText();
		ARG newArg;
		switch (type) {
		case "int":
			if (arg) {
				newArg = new ARG(var, Type.ARRAY_OF_INT(bounds.size()), bounds);
				currentTDS.put(var, newArg);
				f.addArg(newArg);
			} else
				currentTDS.put(var, new IDF(var, Type.ARRAY_OF_INT(bounds.size()), bounds));
			break;
		case "bool":
			if (arg) {
				newArg = new ARG(var, Type.ARRAY_OF_BOOL(bounds.size()), bounds);
				currentTDS.put(var, newArg);
				f.addArg(newArg);
			} else
				currentTDS.put(var, new IDF(var, Type.ARRAY_OF_BOOL(bounds.size()), bounds));
			;
			break;
		}
	}

	public TDS getCurrentTDS() {
		// TODO Auto-generated method stub
		return currentTDS;
	}

	public void updateCurrent(int depth) {
		// TODO Auto-generated method stub
		while (depth <= currentTDS.getDepth() && currentTDS.getFather() != null) {
			currentTDS = currentTDS.getFather();
		}

	}
}

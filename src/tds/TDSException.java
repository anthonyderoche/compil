package tds;

@SuppressWarnings("serial")
public class TDSException extends RuntimeException {

	public TDSException(String message) {
		super(message);
	}

}

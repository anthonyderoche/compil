package ast;

import java.util.ArrayList;
import java.util.List;

import tds.TDS;

public class Node {

	protected List<Node> children;

	private String label, value;

	private int depth;
	private int line;
	private TDS TDS;
	private Node father;

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public Node(String label, String value, int line) {
		this();
		this.label = label;
		this.value = value;
		this.line = line;
	}

	public List<Node> getChildren() {
		return children;
	}

	public Node getChild(int i) {
		return children.get(i);
	}

	public int getLine() {
		return line;
	}

	public Node getChildRecur(int... is) {
		Node child = this;
		for (int i = 0; i < is.length; i++) {
			child = child.getChild(is[i]);
		}
		return child;
	}

	public Node getLastChild() {
		return children.get(getChildCount() - 1);
	}

	public int getChildCount() {
		return children.size();
	}

	public Node() {
		children = new ArrayList<Node>();
		father=null;
	}

	public void addChild(Node child) {
		child.father=this;
		children.add(child);
	}

	public String toString(int depth) {

		String s = value + "\n";
		if (depth >= 0) {
			for (Node n : children) {
				if (n == null)
					s += "VIDE ; ";
				else
					s += n.toString(depth - 1) + " ; ";
			}
		}
		return s;
	}

	public String toString() {
		return this.toString(0);
	}

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return value;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public TDS getTDS() {
		return TDS;
	}

	public void setTDS(TDS tDS) {
		TDS = tDS;
	}

	public Node getFather() {
		return father;
	}

	public void setFather(Node father) {
		this.father = father;
	}

}

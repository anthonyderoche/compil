package ast;

@SuppressWarnings("serial")
public class ASTException extends Exception {
	public ASTException(String message) {
		super(message);
	}
}

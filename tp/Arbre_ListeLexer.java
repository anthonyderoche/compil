// $ANTLR 3.3 Nov 30, 2010 12:50:56 Arbre_Liste.g 2014-01-06 15:38:19

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class Arbre_ListeLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int NEWLINE=4;
    public static final int V=5;
    public static final int INT=6;
    public static final int ID=7;
    public static final int WS=8;

    // delegates
    // delegators

    public Arbre_ListeLexer() {;} 
    public Arbre_ListeLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public Arbre_ListeLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "Arbre_Liste.g"; }

    // $ANTLR start "T__9"
    public final void mT__9() throws RecognitionException {
        try {
            int _type = T__9;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Arbre_Liste.g:3:6: ( '(' )
            // Arbre_Liste.g:3:8: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__9"

    // $ANTLR start "T__10"
    public final void mT__10() throws RecognitionException {
        try {
            int _type = T__10;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Arbre_Liste.g:4:7: ( '.' )
            // Arbre_Liste.g:4:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__10"

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Arbre_Liste.g:5:7: ( ')' )
            // Arbre_Liste.g:5:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Arbre_Liste.g:6:7: ( ',' )
            // Arbre_Liste.g:6:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "V"
    public final void mV() throws RecognitionException {
        try {
            int _type = V;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Arbre_Liste.g:18:3: ( INT | 'nil' )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>='0' && LA1_0<='9')) ) {
                alt1=1;
            }
            else if ( (LA1_0=='n') ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // Arbre_Liste.g:18:5: INT
                    {
                    mINT(); 
                    System.out.print($V.value);

                    }
                    break;
                case 2 :
                    // Arbre_Liste.g:19:5: 'nil'
                    {
                    match("nil"); 

                    System.out.print("nil");

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "V"

    // $ANTLR start "ID"
    public final void mID() throws RecognitionException {
        try {
            int _type = ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Arbre_Liste.g:22:5: ( ( 'a' .. 'z' | 'A' .. 'Z' )+ )
            // Arbre_Liste.g:22:9: ( 'a' .. 'z' | 'A' .. 'Z' )+
            {
            // Arbre_Liste.g:22:9: ( 'a' .. 'z' | 'A' .. 'Z' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='A' && LA2_0<='Z')||(LA2_0>='a' && LA2_0<='z')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Arbre_Liste.g:
            	    {
            	    if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ID"

    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Arbre_Liste.g:23:5: ( ( '0' .. '9' )+ )
            // Arbre_Liste.g:23:9: ( '0' .. '9' )+
            {
            // Arbre_Liste.g:23:9: ( '0' .. '9' )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // Arbre_Liste.g:23:9: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "INT"

    // $ANTLR start "NEWLINE"
    public final void mNEWLINE() throws RecognitionException {
        try {
            int _type = NEWLINE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Arbre_Liste.g:24:8: ( ( '\\r' )? '\\n' )
            // Arbre_Liste.g:24:9: ( '\\r' )? '\\n'
            {
            // Arbre_Liste.g:24:9: ( '\\r' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='\r') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // Arbre_Liste.g:24:9: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "NEWLINE"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Arbre_Liste.g:25:5: ( ( ' ' | '\\t' )+ )
            // Arbre_Liste.g:25:9: ( ' ' | '\\t' )+
            {
            // Arbre_Liste.g:25:9: ( ' ' | '\\t' )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0=='\t'||LA5_0==' ') ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // Arbre_Liste.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // Arbre_Liste.g:1:8: ( T__9 | T__10 | T__11 | T__12 | V | ID | INT | NEWLINE | WS )
        int alt6=9;
        alt6 = dfa6.predict(input);
        switch (alt6) {
            case 1 :
                // Arbre_Liste.g:1:10: T__9
                {
                mT__9(); 

                }
                break;
            case 2 :
                // Arbre_Liste.g:1:15: T__10
                {
                mT__10(); 

                }
                break;
            case 3 :
                // Arbre_Liste.g:1:21: T__11
                {
                mT__11(); 

                }
                break;
            case 4 :
                // Arbre_Liste.g:1:27: T__12
                {
                mT__12(); 

                }
                break;
            case 5 :
                // Arbre_Liste.g:1:33: V
                {
                mV(); 

                }
                break;
            case 6 :
                // Arbre_Liste.g:1:35: ID
                {
                mID(); 

                }
                break;
            case 7 :
                // Arbre_Liste.g:1:38: INT
                {
                mINT(); 

                }
                break;
            case 8 :
                // Arbre_Liste.g:1:42: NEWLINE
                {
                mNEWLINE(); 

                }
                break;
            case 9 :
                // Arbre_Liste.g:1:50: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA6 dfa6 = new DFA6(this);
    static final String DFA6_eotS =
        "\5\uffff\1\12\1\7\4\uffff\1\7\1\12";
    static final String DFA6_eofS =
        "\15\uffff";
    static final String DFA6_minS =
        "\1\11\4\uffff\1\60\1\151\4\uffff\1\154\1\101";
    static final String DFA6_maxS =
        "\1\172\4\uffff\1\71\1\151\4\uffff\1\154\1\172";
    static final String DFA6_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\2\uffff\1\6\1\10\1\11\1\5\2\uffff";
    static final String DFA6_specialS =
        "\15\uffff}>";
    static final String[] DFA6_transitionS = {
            "\1\11\1\10\2\uffff\1\10\22\uffff\1\11\7\uffff\1\1\1\3\2\uffff"+
            "\1\4\1\uffff\1\2\1\uffff\12\5\7\uffff\32\7\6\uffff\15\7\1\6"+
            "\14\7",
            "",
            "",
            "",
            "",
            "\12\5",
            "\1\13",
            "",
            "",
            "",
            "",
            "\1\14",
            "\32\7\6\uffff\32\7"
    };

    static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
    static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
    static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
    static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
    static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
    static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
    static final short[][] DFA6_transition;

    static {
        int numStates = DFA6_transitionS.length;
        DFA6_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
        }
    }

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = DFA6_eot;
            this.eof = DFA6_eof;
            this.min = DFA6_min;
            this.max = DFA6_max;
            this.accept = DFA6_accept;
            this.special = DFA6_special;
            this.transition = DFA6_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__9 | T__10 | T__11 | T__12 | V | ID | INT | NEWLINE | WS );";
        }
    }
 

}
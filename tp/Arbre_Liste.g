grammar Arbre_Liste;

prog : a NEWLINE
  ;

a : V
  | '(' {System.out.println("(");} a x 
  ;  

x : '.' {System.out.print(".");} a ')' {System.out.print(")");}
  | s ')' {System.out.print(")");}
  ;

s : ',' {System.out.print(".(");} a s {System.out.print(")");}
  | {System.out.print(".nil");}
  ;

V : INT {System.out.print($V.value);}
  | 'nil' {System.out.print("nil");}
  ;

ID  :   ('a'..'z'|'A'..'Z')+ ;
INT :   '0'..'9'+ ;
NEWLINE:'\r'? '\n' ;
WS  :   (' '|'\t')+ {$channel=HIDDEN;} ;
// $ANTLR 3.3 Nov 30, 2010 12:50:56 Arbre_Liste.g 2014-01-06 15:38:18

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class Arbre_ListeParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "NEWLINE", "V", "INT", "ID", "WS", "'('", "'.'", "')'", "','"
    };
    public static final int EOF=-1;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int NEWLINE=4;
    public static final int V=5;
    public static final int INT=6;
    public static final int ID=7;
    public static final int WS=8;

    // delegates
    // delegators


        public Arbre_ListeParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public Arbre_ListeParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return Arbre_ListeParser.tokenNames; }
    public String getGrammarFileName() { return "Arbre_Liste.g"; }



    // $ANTLR start "prog"
    // Arbre_Liste.g:3:1: prog : a NEWLINE ;
    public final void prog() throws RecognitionException {
        try {
            // Arbre_Liste.g:3:6: ( a NEWLINE )
            // Arbre_Liste.g:3:8: a NEWLINE
            {
            pushFollow(FOLLOW_a_in_prog10);
            a();

            state._fsp--;

            match(input,NEWLINE,FOLLOW_NEWLINE_in_prog12); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "prog"


    // $ANTLR start "a"
    // Arbre_Liste.g:6:1: a : ( V | '(' a x );
    public final void a() throws RecognitionException {
        try {
            // Arbre_Liste.g:6:3: ( V | '(' a x )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==V) ) {
                alt1=1;
            }
            else if ( (LA1_0==9) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // Arbre_Liste.g:6:5: V
                    {
                    match(input,V,FOLLOW_V_in_a23); 

                    }
                    break;
                case 2 :
                    // Arbre_Liste.g:7:5: '(' a x
                    {
                    match(input,9,FOLLOW_9_in_a29); 
                    System.out.println("(");
                    pushFollow(FOLLOW_a_in_a33);
                    a();

                    state._fsp--;

                    pushFollow(FOLLOW_x_in_a35);
                    x();

                    state._fsp--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "a"


    // $ANTLR start "x"
    // Arbre_Liste.g:10:1: x : ( '.' a ')' | s ')' );
    public final void x() throws RecognitionException {
        try {
            // Arbre_Liste.g:10:3: ( '.' a ')' | s ')' )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==10) ) {
                alt2=1;
            }
            else if ( ((LA2_0>=11 && LA2_0<=12)) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // Arbre_Liste.g:10:5: '.' a ')'
                    {
                    match(input,10,FOLLOW_10_in_x49); 
                    System.out.print(".");
                    pushFollow(FOLLOW_a_in_x53);
                    a();

                    state._fsp--;

                    match(input,11,FOLLOW_11_in_x55); 
                    System.out.print(")");

                    }
                    break;
                case 2 :
                    // Arbre_Liste.g:11:5: s ')'
                    {
                    pushFollow(FOLLOW_s_in_x63);
                    s();

                    state._fsp--;

                    match(input,11,FOLLOW_11_in_x65); 
                    System.out.print(")");

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "x"


    // $ANTLR start "s"
    // Arbre_Liste.g:14:1: s : ( ',' a s | );
    public final void s() throws RecognitionException {
        try {
            // Arbre_Liste.g:14:3: ( ',' a s | )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==12) ) {
                alt3=1;
            }
            else if ( (LA3_0==11) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // Arbre_Liste.g:14:5: ',' a s
                    {
                    match(input,12,FOLLOW_12_in_s78); 
                    System.out.print(".(");
                    pushFollow(FOLLOW_a_in_s82);
                    a();

                    state._fsp--;

                    pushFollow(FOLLOW_s_in_s84);
                    s();

                    state._fsp--;

                    System.out.print(")");

                    }
                    break;
                case 2 :
                    // Arbre_Liste.g:15:5: 
                    {
                    System.out.print(".nil");

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "s"

    // Delegated rules


 

    public static final BitSet FOLLOW_a_in_prog10 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_NEWLINE_in_prog12 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_V_in_a23 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_9_in_a29 = new BitSet(new long[]{0x0000000000000220L});
    public static final BitSet FOLLOW_a_in_a33 = new BitSet(new long[]{0x0000000000001C00L});
    public static final BitSet FOLLOW_x_in_a35 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_10_in_x49 = new BitSet(new long[]{0x0000000000000220L});
    public static final BitSet FOLLOW_a_in_x53 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_x55 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_s_in_x63 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_x65 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_s78 = new BitSet(new long[]{0x0000000000000220L});
    public static final BitSet FOLLOW_a_in_s82 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_s_in_s84 = new BitSet(new long[]{0x0000000000000002L});

}